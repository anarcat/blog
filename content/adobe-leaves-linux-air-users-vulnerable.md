Title: Adobe leaves Linux AIR users vulnerable
Date: 2011-11-21 17:41
Author: Daniel Kahn Gillmor (dkg)
Slug: adobe-leaves-linux-air-users-vulnerable

A few months ago, [Adobe announced a slew of vulnerabilities in its
Flash
Player](https://www.adobe.com/support/security/bulletins/apsb11-12.html),
which is a critical component of Adobe AIR:

> Adobe recommends users of Adobe AIR 2.6.19140 and earlier versions for
> Windows, Macintosh and Linux update to Adobe AIR 2.7.0.1948.
>
> \[...\]
>
> <p>
> June 14, 2011 - Bulletin updated with information on Adobe AIR

However, looking at [Adobe's instructions for installing AIR on "Linux"
systems](http://kb2.adobe.com/cps/408/kb408084.html), we see that it is
impossible for people running a free desktop OS to follow Adobe's own
recommendations:

> Beginning June 14 2011, Adobe AIR is no longer supported for desktop
> Linux distributions. Users can install and run AIR 2.6 and earlier
> applications but can't install or update to AIR 2.7. The last version
> to support desktop Linux distributions is AIR 2.6.

So on the exact same day, Adobe said "we recommend you upgrade, as the
version you are using is vulnerable" and "we offer you no way to
upgrade".

I'm left with the conclusion that Adobe's aggregate corporate message is
"users of desktops based on free software should immediately uninstall
AIR and stop using it".

If Adobe's software was free, and they had a community around it, they
could turn over support to the community if they found it too
burdensome. Instead, once again, users of proprietary tools on free
systems get screwed by the proprietary vendor.

And they wonder why we tend to be less likely to install their tools?

Application developers should avoid targeting AIR as a platform if they
want to reach everyone.

**Tags**: [adobe](https://debian-administration.org/tag/adobe),
[proprietary
software](https://debian-administration.org/tag/proprietary%20software),
[security](https://debian-administration.org/tag/security)

</p>

