Title: Publicly-funded knowledge should be public
Date: 2009-03-17 18:18
Author: Daniel Kahn Gillmor (dkg)
Slug: publicly-funded-knowledge-should-be-public

I live in the USA. Our government issues many grants to scientists for
research via the [National Institute of Health](http://nih.gov). I
recently found out about the NIH's recent requirement that
[publicly-funded research must be published freely online within 12
months](http://publicaccess.nih.gov/). As you can imagine, i think this
is a remarkably Good Thing (though 12 months seems a little bit long for
fast-moving fields).

Apparently, [John Conyers](http://conyers.house.gov/) and several
[co-sponsors](http://thomas.loc.gov/cgi-bin/bdquery/z?d111:HR00801:@@@P)
have introduced [HR
801](http://thomas.loc.gov/cgi-bin/query/z?c111:H.R.801:), which appears
intended to overturn this remarkable policy, primarily for the benefit
of the companies that publish scientific journals.

This bill is a shame, and i had hoped for better from Rep. Conyers, who
otherwise has a
[remarkably](http://en.wikipedia.org/wiki/United_States_National_Health_Insurance_Act)
[positive](http://en.wikipedia.org/wiki/Downing_Street_memo)
[record](http://en.wikipedia.org/wiki/Nixon%27s_Enemies_List#Verbatim_text_of_Colson.27s_original_memo_.28with_his_comments.29)
as a legislator advocating for government transparency and the public
good. Sadly, his stance on so-called "Intellectual Property" seems
characterized by [heavy-handed
legislation](http://en.wikipedia.org/wiki/PRO-IP_Act) designed to
benefit the parties already heavily favored by the current [imbalanced
copyright](http://citesandinsights.info/v6i8e.htm) situation.

If you live in the US (and especially if you live in Conyers' district
in Michigan), please [send him
e-mail](mailto:john.conyers@mail.house.gov?subject=Please+Abandon+HR+801)
or [get in touch by
phone](http://conyers.house.gov/index.cfm?FuseAction=Contact.ContactInformation)
and tell him to drop the bill. You might also check the [list of
cosponsors](http://thomas.loc.gov/cgi-bin/bdquery/z?d111:HR00801:@@@P)
to see if one of them is more local to you.

If you want to read more, [Lawrence Lessig](http://lessig.org/) has
written about this issue, [addressing Congressman Conyers
directly](http://www.huffingtonpost.com/lawrence-lessig/a-reply-to-congressman-co_b_173030.html)
in the Huffington Post. Curiously, Rep. Conyers' web site contains [no
mention of HR
801](http://conyers.house.gov/index.cfm?FuseAction=Search.Results&Keywords=801).

**Tags**: [policy](https://debian-administration.org/tag/policy)

</p>

