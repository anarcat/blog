Title: gpg --ask-cert-level considered harmful
Date: 2013-05-20 07:21
Author: Daniel Kahn Gillmor (dkg)
Slug: gpg-ask-cert-level-considered-harmful

Occasionally, someone asks me whether we should encourage use of the
`--ask-cert-level` option when certifying OpenPGP keys with `gpg`. I see
no good reason to use this option, and i think we should discourage
people from trying to use it. I don't think there is a satisfactory
answer to the question "how will specifying the level of identity
certification concretely benefit anyone involved?", and i don't see why
we should want one.

`gpg` gets it absolutely right by not asking users this question by
default. People should not be enabling this option.

Some background: `gpg`'s `--ask-cert-level` option allows the user who
is making an OpenPGP identity certification to indicate just how sure
they are of the identity they are certifying. The user's choice is then
mapped into four levels of OpenPGP certification of a User ID and
Public-Key packet, which i'll refer to by [their signature type
identifiers in the OpenPGP
spec](https://tools.ietf.org/html/rfc4880#page-20):

> 0x10: Generic certification
> :   The issuer of this certification does not make any particular
>     assertion as to how well the certifier has checked that the owner
>     of the key is in fact the person described by the User ID.
>
> 0x11: Persona certification
> :   The issuer of this certification has not done any verification of
>     the claim that the owner of this key is the User ID specified.
>
> 0x12: Casual certification
> :   The issuer of this certification has done some casual verification
>     of the claim of identity.
>
> 0x13: Positive certification
> :   The issuer of this certification has done substantial verification
>     of the claim of identity.
>
> <p>
> Most OpenPGP implementations make their "key signatures" as 0x10
> certifications. Some implementations can issue 0x11-0x13
> certifications, but *few differentiate between the types*.

By default (if `--ask-cert-level` is not supplied), `gpg` issues
certificates ("signs keys") using 0x10 (generic) certifications, with
the exception of self-sigs, which are made as type 0x13 (positive).

When interpreting certifications, `gpg` does distinguish between
different certifications in one particular way: 0x11 (persona)
certifications are ignored; other certifications are not. (users can
change this cutoff with the `--min-cert-level` option, but it's not
clear why they would want to do so).

So there is no functional gain in declaring the difference between a
"normal" certification and a "positive" one, even if there were a
well-defined standard by which to assess the difference between the
"generic" and "casual" or "positive" levels; and if you're going to make
a "persona" certification, you might as well not make one at all.

And it gets worse: the problem is not just that such an indication is
functionally useless; encouraging people to make these kind of
assertions actively encourages leaks of a more-detailed social graph
than just encouraging everyone to use the default blanket
0x13-for-self-sigs, 0x10-for-everyone-else policy.

A richer public social graph means more data that can feed the ravenous
and growing appetite of the advertising-and-surveillance regimes. i find
these regimes troubling. I admit that people often leak much more
information than this indication of "how well do you know X" via tools
like Facebook, but that's no excuse to encourage them to leak still more
or to acclimatize people to the idea that the details of their personal
relationships should by default be public knowledge.

Lastly, the more we keep the OpenPGP network of identity certifications
(a.k.a. the "web of trust") simple, the easier it is to make sensible
and comprehensible and predictable inferences from the network about
whether a key really does belong to a given user. Minimizing the
complexity and difficulty of deciding to make a certification helps
people streamline their signing processes and reduces the amount of
cognitive overhead people spend just building the network in the first
place.

**Tags**: [openpgp](https://debian-administration.org/tag/openpgp)

</p>

