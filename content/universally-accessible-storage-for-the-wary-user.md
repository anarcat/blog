Title: universally accessible storage for the wary user
Date: 2013-01-09 01:12
Author: Daniel Kahn Gillmor (dkg)
Slug: universally-accessible-storage-for-the-wary-user

A friend wrote me a simple question today. My response turned out to be
longer than i expected, but i hope it's useful (and maybe other people
will have better suggestions) so i thought i'd share it here too:

Angela Starita wrote:

> I'd like to save my work in a location where I can access it from any
> computer. I'm wary of using the mechanisms provided by Google and
> Apple. Can you suggest another service?

Here's my reply:

I think you're right to be wary of the big cloud providers, who have a
tendency to inspect your data to profile you, to participate in
arbitrary surveillance regimes, and to try to sell your eyeballs to
advertisers.

### Caveat: You have to trust the client machine too

But it's also worth remembering that the network service provider is not
the only source of risk. If you really mean "accessing your data from
any computer", that means the computer you're using to access the data
can do whatever it wants with it. That is, you need to trust both the
operator of these "cloud" services, \*and\* the administrator/operating
system of the client computer you're using to access your data. For
example, if you log into any "secure" account from a terminal in a web
café, that leaves you vulnerable to the admins of the web café (and, in
the rather-common case of sloppily-administered web terminals,
vulnerable to the previous user(s) of the terminal as well).

### Option 0: Portable physical storage

One way to have your data so that you can access it from "any computer"
is to not rely on the network at all, but rather to carry a
high-capacity MicroSD card (and USB adapter) around with you (you'll
probably want to format the card with a widely-understood filesystem
like FAT32 instead of NTFS or HFS+ or ext4, which are only understood by
some of the major operating systems, but not all).

Here is some example hardware:

-   [MicroSD USB adapter (\$5)](https://www.adafruit.com/products/939)
-   [16GB class 10 MicroSD card
    (\$20)](http://www.jr.com/pny/pe/PNY_PSDU16G10/)

Almost every computer these days has either a microSD slot or a USB
port, while some computers are not connected to the network. This also
means that you don't have to rely on someone else to manage servers that
keep your data available all the time.

Note that going the microSD route doesn't remove the caveat about
needing to trust the client workstation you're using, and it has another
consideration:

You'd be responsible for your own backup in the case of hardware
failure. You're responsible for your own backup in the case of online
storage too, of course -- but the better online companies are probably
better equipped than most of us to deal with hardware failure. OTOH,
they're also susceptible to some data loss scenarios that we aren't as
individual humans (e.g. the company might go bankrupt, or get bought by
a competitor who wants to terminate the service, or have a malicious
employee who decides to take revenge). Backup of a MicroSD card isn't
particularly hard, though: just get a USB stick that's the same size,
and regularly duplicate the contents of the MicroSD card to the USB
stick.

One last consideration is storage size -- MicroSD cards are currently
limited to 32GB or 64GB. If you have significantly more data than that,
this approach might not be possible, or you might need to switch to a
USB hard disk, which would limit your ability to use the data on
computers that don't have a USB port (such as some smartphones).

### Option 1: Proprietary service providers

If you don't think this portable physical storage option is the right
choice for you, here are a couple proprietary service providers who
offer some flavor of "cloud" storage while claiming to not look at the
contents of your data:

-   [wuala](https://wuala.com/)
-   [spideroak](https://spideroak.com/)

I'm not particularly happy with either of those, though, in part because
the local client software they want you to run is proprietary, so
there's no way to verify that they are actually unable to access the
contents of your data. But i'd be a lot happier with either wuala or
spideroak than i would be with google drive, dropbox, or iCloud.

### Option 2: What i really want

I'm much more excited about the network-accessible, free-software,
privacy-sensitive network-based storage tool known as [git-annex
assistant](http://git-annex.branchable.com/assistant/). The project is
spearheaded by Joey Hess, who is one of the most skilled and thoughtful
software developers i know of.

"assistant" (and git-annex, from which it derives) has the advantage of
being pretty agnostic about the backend service (many plugins for many
different cloud providers) and allows you to encrypt your data locally
before sending it to the remote provider. This also means you can put
your encrypted data in more than one provider, so that if one of the
providers fails for some reason, you can be relatively sure that you
have another copy available.

But "assistant" won't be ready for Windows or Android for several months
(builds are available for Linux and Mac OS X now), so i don't know if it
meets the criterion for "accessible from any computer". And, of course,
even with the encryption capabilities, the old caveat about needing to
trust the local client machine still applies.
