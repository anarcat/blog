Title: Liberating Knowledge: A Librarian's Manifesto
Date: 2011-01-06 05:52
Author: Daniel Kahn Gillmor (dkg)
Slug: liberating-knowledge-a-librarians-manifesto

[A friend](http://alycia.brokenja.ws/) just pointed me to [Liberating
Knowledge: A Librarian's Manifesto for
Change](http://www.nea.org/assets/img/PubThoughtAndAction/A10Fister1.pdf)
by [Barbara Fister](https://barbarafister.wordpress.com/), an [academic
librarian](http://homepages.gac.edu/~fister/vita.html) who also happens
to [write mystery novels](http://barbarafister.com/).

She has an excellent perspective on the meaning of libraries, and the
tradeoffs involved with the current societal trend toward privatizing
knowledge through so-called "intellectual property" regulations. In a
great critique of the passivity of academia and libraries in the face of
attempts at intellectual
[enclosure](https://secure.wikimedia.org/wikipedia/en/wiki/Enclosure) by
private corporations, she writes:

> This uninformed indifference is laying the groundwork for a new
> tragedy of the commons: a world in which knowledge is turned into
> intellectual property, monetizied, and made artificially scarce.

She closes with a six-point manifesto that begins:

> Liberation bibliography arises out of outrage at the injustice of the
> current system. It’s not about saving money, it’s about the empowering
> nature of knowledge and the belief that it shouldn’t be a luxury good
> for the few.

The article abounds in examples of heinous arrangements in the current
system that seem to be accepted as standard procedure, and clear
thinking about what the actual tradeoffs are (and how we, as a society,
are making them poorly).

If i had one objection, it would be that she neglects to mention
increased surveillance as one of the problems that come with
privatization of knowledge. Our abilities to read privately and
anonymously, and to correspond confidentially are at risk because of
these systems of control.

Anyway, I'd love to see more open allegiances between librarians and
free software folks; the ideals and struggles are very much in parallel.
Go talk to your librarian friends about this stuff today!

**Tags**: [academia](https://debian-administration.org/tag/academia),
[freedom](https://debian-administration.org/tag/freedom), [intellectual
property](https://debian-administration.org/tag/intellectual%20property),
[libraries](https://debian-administration.org/tag/libraries)

</p>

