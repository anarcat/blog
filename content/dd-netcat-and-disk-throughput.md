Title: dd, netcat, and disk throughput
Date: 2009-12-21 06:21
Author: Daniel Kahn Gillmor (dkg)
Slug: dd-netcat-and-disk-throughput

I was trying to dump a large Logical Volume (LV) over ethernet from one
machine to another. I found some behavior which surprised me.

### fun constraints

-   I have only a fairly minimal debian installation on each machine
    (which fortunately includes `netcat-traditional`)
-   The two machines are connected directly by a single (gigabit)
    ethernet cable, with no other network connection. So no pulling in
    extra packages.
-   I have serial console access to both machines, but no physical
    access.
-   The LV being transfered is 973GB in size according to `lvs` (fairly
    large, that is), and contains a LUKS volume, which itself contains a
    basically-full filesystem -- transferring just the "used" bytes is
    not going to save space/time.
-   I want to be able to check on how the transfer is doing while it's
    happening.
-   I want the LV to show up as an LV on the target system, and don't
    have tons of extra room on the target to play around with (so no
    dumping it to the filesystem as a disk image first).

(how do i get myself into these messes?)

This entry has been truncated [read the full
entry](https://debian-administration.org/users/dkg/weblog/57).

**Tags**: [lvm](https://debian-administration.org/tag/lvm),
[netcat](https://debian-administration.org/tag/netcat)

</p>

