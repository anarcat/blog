Title: transient &quot;obsolete packages&quot; with apt
Date: 2008-06-18 20:43
Author: Daniel Kahn Gillmor (dkg)
Slug: transient-obsolete-packages-with-apt

On the testing and unstable systems that i run, i occasionally get a
disturbing "there are *N* newly obsolete packages" message. For example

``` {.terminal}
[0 root@squeak ~]# aptitude updateHit http://cmrg.fifthhorseman.net unstable Release.gpg [...]Get:42 http://ftp.us.debian.org unstable/main [2077kB]                          Fetched 15.2MB in 57s (262kB/s)                                                 Reading package lists... DoneUpdating debtags database...... DoneCurrent status: 3 updates [+3], 1136 new [-21212].There are 1804 newly obsolete packages.[255 root@squeak ~]# 
```

Usually, i can fix this with another `aptitude update`. Does this mean
i've hit a mirror at an inopportune time? Or do i have apt
mis-configured somehow?

[Other people have
documented](http://www.debianhelp.org/node/7602#comment-42130) this
before, and casual conversation with friends lets me know it's not just
me.

If this is due to some inconsistency at the mirrors (particularly for
suites with rolling updates?), is there some way to engineer the mirror
transfers so that this doesn't happen? For example (i have no idea how
this stuff is done, so i'm just making this up):

1.  rsync the `pool/` without `--delete` to all mirrors,
2.  rsync the `Packages` and `Release`, and `DiffIndex` files to all
    mirrors,
3.  rsync the `pool/` *with* `--delete`

Or should apt itself deal with these circumstances differently? It seems
like signed apt ought to be able to detect when something like this is
amiss.

I'm reluctant to consider the situation acceptable, because i'd like to
be able to trust apt in general. In particular, i can imagine apt
eventually becoming more assertive about suggesting removal of newly
obsolete and unsupported packages. If it is wrong about what's obsolete
and unsupported, this could be dangerous.
