Title: The legal utility of deniability in secure chat
Date: 2013-12-05 23:14
Author: Daniel Kahn Gillmor (dkg)
Slug: the-legal-utility-of-deniability-in-secure-chat

This Monday, I attended [a workshop on Multi-party Off the Record
Messaging and
Deniability](https://www.calyxinstitute.org/events/multiparty-otr-and-deniability)
hosted by the Calyx Institute. The discussion was a combination of legal
and technical people, looking at how the characteristics of this
particular technology affect (or do not affect) the law.

This is a report-back, since I know other people wanted to attend. I'm
not a lawyer, but I develop software to improve communications security,
I care about these questions, and I want other people to be aware of the
discussion. I hope I did not misrepresent anything below. I'd be happy
if anyone wants to offer corrections.

Background
----------

[Off the Record Messaging (OTR)](https://otr.cypherpunks.ca/) is a way
to secure instant messaging (e.g. jabber/XMPP, gChat, AIM).

The two most common characteristics people want from a secure instant
messaging program are:

Authentication
:   Each participant should be able to know specifically who the other
    parties are on the chat.

Confidentiality
:   The content of the messages should only be intelligible to the
    parties involved with the chat; it should appear opaque or encrypted
    to anyone else listening in. Note that confidentiality effectively
    depends on authentication -- if you don't know who you're talking
    to, you can't make sensible assertions about confidentiality.

As with many other modern networked encryption schemes, OTR relies on
each user maintaining a long-lived "secret key", and publishing a
corresponding "public key" for their peers to examine. These keys are
critical for providing authentication (and by extension, for
confidentiality).

But OTR offers several interesting characteristics beyond the common
two. Its most commonly cited characteristics are "forward secrecy" and
"deniability".

Forward secrecy
:   Assuming the parties communicating are operating in good faith,
    forward secrecy offers protection against a special kind of
    adversary: one who logs the encrypted chat, and subsequently steals
    either party's long-term secret key. Without forward secrecy, such
    an adversary would be able to discover the content of the messages,
    violating the confidentiality characteristic. With forward secrecy,
    this adversary is be stymied and the messages remain confidential.

Deniability
:   Deniability only comes into play when one of the parties is no
    longer operating in good faith (e.g. their computer is compromised,
    or they are collaborating with an adversary). In this context, if
    Alice is chatting with Bob, she does not want Bob to be able to
    cryptographically prove to anyone else that she made any of the
    specific statements in the conversation. This is the focus of
    Monday's discussion.
    <p>
    To be clear, this kind of deniability means Alice can correctly say
    "you have no cryptographic proof I said *X*", but it does not let
    her assert "here is cryptographic proof that I *did not* say *X*" (I
    can't think of any protocol that offers the latter assertion). The
    opposite of deniability is a cryptographic proof of origin, which
    usually runs something like "only someone with access to Alice's
    secret key could have said *X*."

The traditional two-party OTR protocol has offered both forward secrecy
and deniability for years. But deniability in particular is a
challenging characteristic to provide for group chat which is the domain
of Multi-Party OTR (mpOTR). You can read [some past discussion about the
challenges of deniability in mpOTR (and why it's harder when there are
more than two people
chatting)](http://thread.gmane.org/gmane.comp.security.otr.user/1453)
from [the otr-users mailing
list](http://lists.cypherpunks.ca/mailman/listinfo/otr-users).

If you're not doing anything wrong...
-------------------------------------

The discussion was well-anchored by a comment from another participant
who cheekily asked "If you're not doing anything wrong, why do you need
to hide your chat at all, let alone be able to deny it?"

The general sense of the room was that we'd all heard this question many
times, from many people. There are lots of problems with the ideas
behind the question from many perspectives. But just from a legal
perspective, there are at least two problems with the way this question
is posed:

-   laws themselves are not always just (e.g. consider chat
    communications between an interracial couple in the USA [before
    1967](https://en.wikipedia.org/wiki/Loving_v._Virginia), if instant
    messaging had existed at the time), and
-   law enforcement (or a legal adversary in civil litigation) may have
    a different understanding or interpretation of the law than you do
    (e.g. consider chat communications between a corporate or government
    whistleblower and a journalist).

In these situations, people confront real risk from the law. If we care
about these people, we need to figure out if we can build systems to
help them reduce that legal risk (of course we also need to fix broken
laws, and the legal environment in general, but those approaches were
out of scope for this discussion).

The Legal Utility of Deniability
--------------------------------

Monday's meeting was called specifically because it wasn't clear how
much real-world usefulness there is in the "deniability" characteristic,
and whether this feature is worth the development effort and
implementation tradeoffs required. In particular, the group was
interested in deniability's utility in legal contexts; many (most?)
people in the room were lawyers, and it's also not clear that
deniability has much utility outside of a formal legal setting. If your
adversary isn't constrained by some rule of law, they probably won't
care at all whether there is a cryptographic proof or not that you wrote
a particular message (In retrospect, one possible exception is exposure
in the media, but we did not discuss that scenario).

### Places of possible usefulness

So where might deniability come in handy during civil litigation or a
criminal trial? Presumably the circumstance is that a piece of a chat
log is offered as incriminating evidence, and the defendant is trying to
deny something that they appear to have said in the log.

This denial could take place in two rather different contexts: during
rules over admissibility of evidence, or (once admitted) in front of a
jury.

In legal wrangling over admissibility, apparently a lot of horse-trading
can go on -- each side concedes some things in exchange for the other
side conceding other things. It appears that cryptographic proof of
origin (that is, a *lack* of deniability) on the chat logs themselves
might reduce the amount of leverage a defense lawyer can get from
conceding or arguing strongly over that piece of evidence. For example,
if the chain of custody of a chat transcript is fuzzy (i.e. the
transcript could have been mishandled or modified somehow before
reaching trial), then a cryptographic proof of origin would make it much
harder for the defense to contest the chat transcript on the grounds of
tampering. Deniability would give the defense more bargaining power.

In arguing about already-admitted evidence before a jury, deniability in
this sense seems like a job for expert witnesses, who would need to
convince the jury of their interpretation of the data. There was a lot
of skepticism in the room over this, both around the possibility of most
jurors really understanding what OTR's claim of deniability actually
means, and on jurors' ability to distinguish this argument from a bogus
argument presented by an opposing expert witness who is willing to lie
about the nature of the protocol (or who misunderstands it and passes on
their misunderstanding to the jury).

The complexity of the tech systems involved in a data-heavy prosecution
or civil litigation are themselves opportunities for lawyers to argue
(and experts to weigh in) on the general reliability of these systems.
Sifting through the quantities of data available and ensuring that the
appropriate evidence is actually findable, relevant, and suitably
preserved for the jury's inspection is a hard and complicated job, with
room for error. OTR's deniability might be one more element in a
multi-pronged attack on these data systems.

These are the most compelling arguments for the legal utility of
deniability that I took away from the discussion. I confess that they
don't seem particularly strong to me, though some level of "avoiding a
weaker position when horse-trading" resonates with me.

What about the arguments against its utility?

### Limitations

The most basic argument against OTR's deniability is that courts don't
care about cryptographic proof for digital evidence. People are
convicted or lose civil cases based on unsigned electronic
communications (e.g. normal e-mail, plain chat logs) all the time. OTR's
deniability doesn't provide any legal cover stronger than trying to
claim you didn't write a given e-mail that appears to have originated
from your account. As someone who understands the forgeability of
e-mail, i find this overall situation troubling, but it seems to be
where we are.

Worse, OTR's deniability doesn't cover *whether* you had a conversation,
just *what you said* in that conversation. That is, Bob can still
cryptographically prove to an adversary (or before a judge or jury) that
he had a communication with someone controlling Alice's secret key
(which is probably Alice); he just can't prove that Alice herself said
any particular part of the conversation he produces.

Additionally, there are runtime tradeoffs depending on how the protocol
manages to achieve these features. For example, forward secrecy itself
requires an additional round trip or two when compared to authenticated,
encrypted communications without forward secrecy (a "round trip" is a
message from Alice to Bob followed by a message back from Bob to Alice).

Getting proper deniability into the mpOTR spec might incur extra latency
(imagine having to wait 60 seconds after everyone joins before starting
a group chat, or a pause in the chat of 15 seconds when a new member
joins) or extra computational power (meaning that they might not work
well on slower/older devices) or an order of magnitude more bandwidth
(meaning that chat might not work at all on a weak connection). There
could also simply be complexity that makes it harder to correctly
implement a protocol with deniability than an alternate protocol without
deniability. Incorrectly-implemented software can put its users at risk.

I don't know enough about the current state of mpOTR to know what the
specific tradeoffs are for the deniability feature, but it's clear there
will be some. Who decides whether the tradeoffs are worth the feature?

### Other kinds of deniability

Further weakening the case for the legal utility of OTR's deniability,
there seem to be other ways to get deniability in a legal context over a
chat transcript.

There are deniability arguments that can be made from outside the
protocol. For example, you can always claim someone else took control of
your computer while you were asleep or using the bathroom or eating
dinner, or you can claim that your computer had a virus that exported
your secret key and it must have been used by someone else.

If you're desperate enough to sacrifice your digital identity, you could
arrange to have your secret key published, at which point anyone can
make signed statements with it. Having forward secrecy makes it possible
to expose your secret key without exposing the content of your past
communications to any listener who happened to log them.

### Conclusion

My takeaway from the discussion is that the legal utility of OTR's
deniability is non-zero, but quite low; and that development energy
focused on deniability is probably only justified if there are very few
costs associated with it.

Several folks pointed out that most communications-security tools are
too complicated or inconvenient to use for normal people. If we have
limited development energy to spend on securing instant messaging,
usability and ubiquity would be a better focus than this form of
deniability.

Secure chat systems that take too long to make, that are too complex, or
that are too cumbersome are not going to be adopted. But this doesn't
mean people won't chat at all -- they'll just use cleartext chat, or
maybe they'll use supposedly "secure" protocols with even worse
properties: for example, without proper end-to-end authentication
(permitting spoofing or impersonation by the server operator or
potentially by anyone else); with encryption that is reversible by the
chatroom operator or flawed enough to be reversed by any listener with a
powerful computer; without forward secrecy; or so on.

As a demonstration of this, we heard some lawyers in the room admit to
using Skype to talk with their clients *even though they know it's not a
safe communications channel because their clients' adversaries might
have access to the skype messaging system itself*.

My conclusion from the meeting is that there are a few particular
situations where deniability could be useful legally, but that overall,
it is not where we as a community should be spending our development
energy. Perhaps in some future world where all communications are
already authenticated, encrypted, and forward-secret by default, we can
look into improving our protocols to provide this characteristic, but
for now, we really need to work on usability, popularization, and wide
deployment.

### Thanks

Many thanks to [Nick
Merrill](https://www.calyxinstitute.org/about/board/nicholas-merrill)
for organizing the discussion, to [Shayana
Kadidal](http://ccrjustice.org/about-us/staff-board/kadidal,-shayana)
and [Stanley Cohen](http://istanleycohen.org/) for providing a wealth of
legal insight and legal experience, to [Tom Ritter](http://ritter.vg/)
for an excellent presentation of the technical details, and to everyone
in the group who participated in the interesting and lively discussion.

**Tags**: [chat](https://debian-administration.org/tag/chat),
[deniability](https://debian-administration.org/tag/deniability),
[otr](https://debian-administration.org/tag/otr),
[security](https://debian-administration.org/tag/security)

</p>

