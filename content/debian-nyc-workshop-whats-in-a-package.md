Title: Debian NYC Workshop: What's in a Package?
Date: 2010-10-20 21:31
Author: Daniel Kahn Gillmor (dkg)
Slug: debian-nyc-workshop-whats-in-a-package

Debian NYC will be holding a workshop next week: [What's in a
Package?](https://wiki.debian.org/DebianNYC/Workshops/WhatsInAPackage)
will happen at 7:00pm New York time on October 27, 2010. If you're in
the New York area, interested in packaging things for debian and related
systems, or just want to understand the packages in your system better,
you should RSVP and come on out!

This workshop will provide advanced theory useful for people modifying
or creating packages. For people modifying packages, you'll learn many
typical motifs and about various build systems. For creating packages,
you'll be much better prepared to read and understand guides a deep
level. However, this is still not a step-by-step guide in "how to build
packages", but will get you very close to there.

See you there!
