Title: interrupt (IRQ) prioritization under debian?
Date: 2007-11-05 07:59
Author: Daniel Kahn Gillmor (dkg)
Slug: interrupt-irq-prioritization-under-debian

Does anyone have pointers for how to ask a modern debian system to
prioritize certain interrupts (IRQs) ahead of others?

`irqtune` is the historical option, but it hasn't been updated for 10
years, and looks [like a serious mess in
debian](http://bugs.debian.org/cgi-bin/pkgreport.cgi?src=hwtools) at the
moment (it's not in etch or lenny, orphaned, and has many serious bugs
open).

Any thoughts or suggestions for what might be a better way to prioritize
interrupts on a modern system?

**Tags**: [hardware](https://debian-administration.org/tag/hardware),
[interrupt](https://debian-administration.org/tag/interrupt),
[irq](https://debian-administration.org/tag/irq),
[prioritization](https://debian-administration.org/tag/prioritization)

</p>

