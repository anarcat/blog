Title: TLS Infrastructure frustrations (part 1)
Date: 2006-11-25 00:57
Author: Daniel Kahn Gillmor (dkg)
Slug: tls-infrastructure-frustrations-part-1

Prompted by [Steve's request for an SSL
cert](https://debian-administration.org/articles/465), i'm going to air
some grievances i have with the X.509 PKI that tends to go along with
SSL and TLS. The current real-world X.509/TLS infrastructure gets in the
way of real trusted, secure communication. It favors the creation of
opaque, commercialized certificate authorities which are answerable to
no one and have incentives to behave in un-trustworthy ways.

A [small change in
infrastructure](http://www.ietf.org/internet-drafts/draft-ietf-tls-openpgp-keys-11.txt)
to allow multiple signatures in each certificate could move us in the
direction of more trustworthy, reliable digital communication. User
education about digital trust would help too, of course, but i'm not
naive enough to think that'll happen soon.

This entry has been truncated [read the full
entry](https://debian-administration.org/users/dkg/weblog/12).
