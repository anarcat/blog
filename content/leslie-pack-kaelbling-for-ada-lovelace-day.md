Title: Leslie Pack Kaelbling for Ada Lovelace Day
Date: 2009-03-25 06:27
Author: Daniel Kahn Gillmor (dkg)
Slug: leslie-pack-kaelbling-for-ada-lovelace-day

So i'm a couple hours late for [Ada Lovelace
Day](http://findingada.com/) (in my time zone at least), but i wanted to
mention [Leslie Pack Kaelbling](http://people.csail.mit.edu/lpk/), an
excellent technologist who has also had a significant impact on my life.

Her extremely [short
biography](http://people.csail.mit.edu/lpk/bio.html) says (in full):

> Leslie Pack Kaelbling is Professor of Computer Science and Engineering
> at the Computer Science and Artificial Intelligence Laboratory (CSAIL)
> at the Massachusetts Institute of Technology. Her research focuses on
> decision-making under uncertainty, learning, and sensing with
> applications to robotics.

She is also the founder and co-editor-in-chief of the [Journal of
Machine Learning Research](http://jmlr.csail.mit.edu/), an active
open-access academic collection which encourages the development of
[open source software for the field of machine
learning](http://jmlr.csail.mit.edu/mloss/). Cool stuff!

`lpk` was my professor when i was an undergraduate (before she went to
MIT), and she opened my mind to a lot of great ideas about artificial
intelligence, machine learning, robotics, and computation in general.
Beyond the specifics that she taught, though, she also demonstrated what
it means to be a spirited and engaged academic, both in research and in
teaching. I think i first read [Stanslaw
Lem](http://wikipedia.org/Stanislaw_Lem) in one of her robotics classes
(it was one of the stories from [The
Cyberiad](http://english.lem.pl/index.php/works/novels/the-cyberiad/57-a-look-inside-the-cyberiad)),
where she encouraged us to think not just about the technical
possibilities of machinery, but also about the social possibilities. And
it wasn't just theorizing: I also had the chance to do significant work
with robotics hardware (both real and emulated) in a lab under her
friendly and open supervision.

She developed a new introductory series of
[two](http://www.cs.brown.edu/courses/csci0170.html)
[classes](http://www.cs.brown.edu/courses/csci0180.html) for the
school's Computer Science department, which i was unfortunately too late
to take. They're designed to reach students who were discouraged by the
traditional programming-heavy approach, and focus instead on the issues
of theory, abstraction, and collaboration, with programming taking a
subordinate place to understanding the nature of computation itself.
Later in my scholastic career, I had the privilege of helping her out as
a teaching assistant, working with her on various robotics projects,
advising a handful of younger students with her, and having her advise
my final-year project, where i tested and evaluated variants of the
[boosting machine-learning
algorithm](http://en.wikipedia.org/wiki/Boosting). Throughout all of
this, and despite my typical collegiate distractions, Leslie helped me
understand technical details and nuance about the possibilities that we
have with these incredible machines, and how they might be made to
interact with the real world with all of its contradictory input and
uncertainty.

So here's to Leslie Pack Kaelbling: thank you for all your amazing
contributions to our field, and for the things you taught me and the
opportunities you shared!

**Tags**:
[adalovelaceday09](https://debian-administration.org/tag/adalovelaceday09)

</p>

