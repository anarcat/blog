Title: automatically have uscan check signatures
Date: 2013-12-18 03:15
Author: Daniel Kahn Gillmor (dkg)
Slug: automatically-have-uscan-check-signatures

If you maintain software in debian, one of your regular maintenance
tasks is checking for new upstream versions, reviewing them, and
preparing them for debian if appropriate. One of those steps is often to
verify the cryptographic signature on the upstream source archive.

At the moment, most maintainers do the cryptographic check manually, or
maybe even don't bother to do it at all. For the common case of detached
OpenPGP signatures, though,
[uscan](http://manpages.debian.org/cgi-bin/man.cgi?query=uscan) can now
do it for you automatically (as of
[devscripts](http://packages.debian.org/devscripts) version 2.13.3). You
just need to tell uscan what keys you expect upstream to be signing
with, and how to find the detached signature.

So, for example, [Damien Miller recently announced his new key that he
will be using to sign OpenSSH
releases](https://lists.mindrot.org/pipermail/openssh-unix-dev/2013-December/031897.html)
(his new key has OpenPGP fingerprint
`59C2 118E D206 D927 E667 EBE3 D3E5 F56B 6D92 0D30` -- you can verify it
has been cross-signed by his older key, and his older key has been
revoked with the indication that it was superceded by this one). Having
done a reasonable verification of Damien's key, if i was the `openssh`
package maintainer, i'd do the following:

    cd ~/src/openssh/mkdir -p debian/upstreamgpg --export-options export-minimal --armor --export '59C2 118E D206 D927 E667  EBE3 D3E5 F56B 6D92 0D30' >> debian/upstream/signing-key.asc

And then upon noticing that the signature files are named with a simple
`.asc` suffix on [the upstream distribution
site](ftp://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/), we can use
the following `pgpsigurlmangle` option in `debian/watch`:

    version=3opts=pgpsigurlmangle=s/$/.asc/ ftp://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-(.*)\.tar\.gz 

I've filed this specific example as [debian bug
\#732441](http://bugs.debian.org/732441). If you notice a package with
upstream signatures that aren't currently being checked by uscan (or if
you are upstream, you sign your packages, and you want your debian
maintainer to verify them), you can file similar bugs. Or, if you
maintain a package for debian, you can just fix up your package so that
this check is there on the next upload.

If you maintain a package whose upstream doesn't sign their releases,
ask them why not -- wouldn't upstream prefer that their downstream users
can verify that each release wasn't tampered with?

Of course, none of these checks take the the place of the real work of a
debian package maintainer: reviewing the code and the changelogs,
thinking about what changes have happened, and how they fit into the
broader distribution. But it helps to automate one of the basic
safeguards we should all be using. Let's eliminate the possibility that
the file was tampered with at the upstream distribution mirror or while
in transit over the network. That way, the maintainer's time and energy
can be spent where they're more needed.

UPDATED 2015-05-03: use the currently-preferred location for the signing
key.

**Tags**: [crypto](https://debian-administration.org/tag/crypto),
[devscripts](https://debian-administration.org/tag/devscripts),
[openpgp](https://debian-administration.org/tag/openpgp), [package
maintenance](https://debian-administration.org/tag/package%20maintenance),
[signatures](https://debian-administration.org/tag/signatures),
[uscan](https://debian-administration.org/tag/uscan)

</p>

