Title: tzconfig failing on xen instances
Date: 2007-08-09 02:31
Author: Daniel Kahn Gillmor (dkg)
Slug: tzconfig-failing-on-xen-instances

I had trouble recently with a few xen etch instances built off the
typical etch xen packages. The problem was that the domUs all reported
time natively in UTC, despite my having used `tzconfig` to set it to my
local timezone (i'm not in UTC).

Within the domU, while i could `cat /etc/timezone` and it would show the
correct zone, doing a
`cmp /etc/localtime /usr/share/zoneinfo/my/time/zone` indicated that
they were in fact different. and tools like \`date\` would report in UTC
if the TZ variable wasn't set.

\` running `dpkg-reconfigure tzdata` did the proper thing, though, and
updated `/etc/localtime`. Had that failed, i would have just copied (or
linked, if `/usr` shared a partition with `/`) in the proper file to
`/etc/localtime`. given that `tzconfig` seems to have been removed from
lenny, i'm not sure where/how to report this problem, or if it's worth
it. It may be due to a weird interaction between tzconfig and xen's
default clock weirdnesses, too (the domU's inherit their clock from the
dom0 unless you deliberately detach them).

**Tags**: [clock](https://debian-administration.org/tag/clock),
[timezones](https://debian-administration.org/tag/timezones),
[tzconfig](https://debian-administration.org/tag/tzconfig),
[xen](https://debian-administration.org/tag/xen)

</p>

