Title: Monkeysphere: an OpenPGP-based PKI for SSH
Date: 2008-09-04 07:13
Author: Daniel Kahn Gillmor (dkg)
Slug: monkeysphere-an-openpgp-based-pki-for-ssh

Ever thought that there should be an automated way to handle ssh keys?
Do you know the administrators of your servers, and wish that SSH could
verify new host keys from them automatically, based on your personal
connections to the web-of-trust? Do you wish you could revoke and/or
rotate your old SSH authentication keys without having to log into every
single machine you have an account on?

Do you administer servers, and wish you could re-key them without sowing
massive confusion among your users (or worse, encouraging bad security
habits among them)? Do you wish you could grant access to your users by
name, instead of by opaque string? Do you wish you could rapidly revoke
access to a user (or compromised key) across a group of machines by
disabling authentication for that user?

A group of us have been working on a public key infrastructure for SSH.
[Monkeysphere](http://web.monkeysphere.info) makes use of the existing
OpenPGP web-of-trust to fetch and cryptographically validate (and
revoke!) keys. This works in both direction: `authorized_keys` *and*
`known_hosts` are handled. Monkeysphere gives users and admins tools to
deal with SSH keys by thinking about the people and machines to whom the
keys belong, instead of requiring humans to do tedious (and error-prone)
manual key verification.

We have [debian packages
available](http://web.monkeysphere.info/download) which should install
against lenny (for i386, amd64, powerpc, and arm architectures at the
moment), [a mailing
list](https://lists.riseup.net/www/info/monkeysphere), and open ears for
good questions, suggestions and criticism.

If you have a chance to give it a try ([as a
user](http://web.monkeysphere.info/getting-started-user/) or [as an
admin](http://web.monkeysphere.info/getting-started-admin/)), it would
be great to [get
feedback](https://lists.riseup.net/www/info/monkeysphere).

**Tags**: [gpg](https://debian-administration.org/tag/gpg),
[monkeysphere](https://debian-administration.org/tag/monkeysphere),
[openpgp](https://debian-administration.org/tag/openpgp),
[openssh](https://debian-administration.org/tag/openssh),
[pgp](https://debian-administration.org/tag/pgp),
[ssh](https://debian-administration.org/tag/ssh)

</p>

