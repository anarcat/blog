Title: tremor-based alsaplayer?
Date: 2006-11-14 20:30
Author: Daniel Kahn Gillmor (dkg)
Slug: tremor-based-alsaplayer

Does anyone know of a simple, clean, ALSA-output ogg-vorbis audio player
that links (statically or otherwise) against the tremor integer-only
vorbis decoder? Here's why i want it:

I recently got a [Linksys NSLU2](http://www.cyrius.com/debian/nslu2/)
which i'm happily running debian etch on (with the sid kernel, thanks a
million to the debian arm team!). “`igor`” is tiny, slow, fanless,
silent, low-power ARM-based NIC/USB platform. It's plugged into a USB
disk, and hooked into my network, NFS-mounting my music from a different
server.

I recently added a [USB audio
adapter](http://www.newegg.com/Product/Product.asp?Item=N82E16829126101),
which is working for basic stereo output. (getting the other 4 audo
channels and the buttons on the USB device to work hasn't happened yet,
but that's another weblog post, probably).

I can easily and simply play back the mp3s that i have stored, but i
found i couldn't play back my ogg vorbis-formatted audio. Watching the
state of the system with

    vmstat 1

showed that playing mp3s (via `alsaplayer`, via `libmad`), \~25% of the
CPU is spent in userland, and \~25% is spent in the kernel, leaving
\~50% free for whatever else i want to do with `igor`. Not too bad for a
wimpy 266Mhz machine with 32 MB of RAM.

However, when i try to play ogg vorbis files, the `vmstat` output is
nasty: \~97% kernel, and the rest in userland. only the choppiest audio
comes out. It's unacceptable, basically. On further review, i'm pretty
sure that the problem is that `libvorbis` is using floating-point math,
while `libmad` uses integer math. i think the NSLU2 lacks a hardware
FPU, and does floating-point emulation in the kernel, which would
account for the stats i've been seeing.

Enter [Tremor](http://en.wikipedia.org/wiki/Tremor_(software)), the
integer-math-only ogg vorbis decoder. It appears that tremor isn't
packaged for debian yet, though i could get [the
source](http://svn.xiph.org/trunk/Tremor) to build pretty easily on
`igor`. I'm willing to take a crack at packaging tremor for debian
(though i don't know anything about library packaging), but i want to
test it out first! i don't have any example players that link against
it. My preferred simple CLI audio player these days is `alsaplayer`, but
it links directly against `libvorbis`, and doesn't appear to know
anything about tremor in the code.

Should i try to modify `alsaplayer`? Is there a simple tremor demo app
that plays via alsa? Is there an `alsaplayer` patchset that i just
missed in my web searches? I'll probably try to dig into this in the
next couple of weeks, but if anyone has any suggestions, i'd appreciate
hearing about them.
