Title: looking for a STARTTLS-capable MANAGESIEVE client
Date: 2006-06-28 05:17
Author: Daniel Kahn Gillmor (dkg)
Slug: looking-for-a-starttls-capable-managesieve-client

I've got a mailserver running a lightly-patched cyrus21, based on debian
sarge.

The mailserver runs (among other things) `timsieved`, and all
connections are STARTTLS-capable (and enforced). But i'm having trouble
finding a useful client that can talk to it properly.

This entry has been truncated [read the full
entry](https://debian-administration.org/users/dkg/weblog/4).
