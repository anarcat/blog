Title: Ensuring system updates are actually in use
Date: 2006-09-10 15:30
Author: Daniel Kahn Gillmor (dkg)
Slug: ensuring-system-updates-are-actually-in-use

I keep my machines all patched with the latest updates from
security.debian.org. But sometimes, a simple
`apt-get update && apt-get dist-upgrade` is not enough, particularly
when system libraries are being upgraded (e.g. the latest openssl
vulnerability, DSA 1173). In this situation, running processes could
have loaded copies of the old libraries, so they wouldn't get the
benefits of the old version. as DSA 1173 says:

> Note that services linking against the openssl shared libraries will
> need to be restarted. Common examples of such services include most
> Mail Transport Agents, SSH servers, and web servers.

</i>

So after an upgrade, it's important to check that the affected services
are really restarted. The best way i've found so far is to check what
deleted files are currently being held open by any process. I use lsof
to look at this:

    root@serverX:~# lsof | grep 'path inode'apache2    9255    www-data  mem       REG        3,3              725883 /usr/lib/i686/cmov/libssl.so.0.9.7 (path inode=725841)apache2    9255    www-data  mem       REG        3,3              725770 /usr/lib/i686/cmov/libcrypto.so.0.9.7 (path inode=725839)root@serverX:~# 

*(note: i've excised some lines of the actual output for brevity)*

I think the files that lsof lists with the (path inode=NNNN) designation
are files being held open by the system after having been removed or
replaced. So seeing the above output lets me know that i need to restart
`apache2` on this particular server. Otherwise, apache2 is still
vulnerable to the openssl vulnerability, despite having the updated
version installed on the system.

Try running this on one of your own systems. You might be surprised at
the services in want of an upgrade, especially if you tracked the latest
r3 of sarge, with its glibc upgrade.

i'm not convinced that my lsof|grep filter is the best way to look up
this information, but it seems to work for me. I'd be interested to know
if other people have better ways to check on the same situation.
