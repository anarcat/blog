Title: Python editor/IDE for new (high school) coders?
Date: 2009-02-04 19:56
Author: Daniel Kahn Gillmor (dkg)
Slug: python-editoride-for-new-high-school-coders

I'm supporting a class of high school students who are new to
programming, and will be learning some python. Most of these students
are comfortable with computers, but not hacker types, and few if any of
them have written code before.

I'm looking for an editor or an Integrated Development Environment (IDE)
that won't be too scary for them, and will help them get used to the
novel idea of writing code without simultaneously having to get used to
the novel idea of an unfamiliar user interface.

So i can't expect them to pick up my beloved emacs, for example. But on
the other end of the spectrum, i'd hate for them to try to write python
in an word processor (i've seen people do it!). Here's what i think i'm
looking for:

-   comfortable graphical interface -- these students have not used the
    command line before, and while they'll be introduced to the python
    shell, they should be able to write code and browse for files, etc.
    in the "normal" (sigh) way. This means, for instance, that tk-based
    interfaces are less good because they don't integrate with the
    common GTK-based UI.
-   syntax highlighting -- the students will need to figure out what's a
    variable, what's a function name, what's an operator, what's a
    reserved word, etc. Visual indications like font-lock-mode from
    emacs would be really useful.
-   whitespace management -- since python has syntactic whitespace, it
    would be great if there were convenient/intuitive ways for students
    to adjust the whitespace in their programs. Of course, it's hard to
    say what's going to be intuitive for other people. I like
    tab-cycling whitespace myself; are there other approaches i should
    be proposing?

Do you have suggestions for tools that work well for the above needs?
Favorites? Are there features other than the "top three" i listed above
that you think i should pay attention to?

FWIW, they'll be working in a labful of machines running Ubuntu Hardy
for the most part, but i'm willing to port and deploy programs
(particularly ones that feel like a perfect fit) if they're not
available in hardy.

**Tags**: [ide](https://debian-administration.org/tag/ide),
[python](https://debian-administration.org/tag/python),
[recommendations](https://debian-administration.org/tag/recommendations),
[student](https://debian-administration.org/tag/student)

</p>

