Title: apt warnings about missing public keys
Date: 2006-11-21 17:56
Author: Daniel Kahn Gillmor (dkg)
Slug: apt-warnings-about-missing-public-keys

After running `apt-get update` on a typically-stable mixed etch/sid
machine (which also has experimental in the sources list), i'm getting
the following warnings from apt:

    W: There are no public key available for the following key IDs:A70DAF536070D3A1W: There are no public key available for the following key IDs:A70DAF536070D3A1W: There are no public key available for the following key IDs:A70DAF536070D3A1W: You may want to run apt-get update to correct these problems

Is anyone else seeing this? a quick glance at google makes me think it's
not very important, but i'd like some confirmation on this. Also, the
identical repeated error messages look odd. If it's one from each
upstream source, shouldn't apt include the message about which source is
generating the error? otherwise, why bother printing the same thing 3
times?

</p>

