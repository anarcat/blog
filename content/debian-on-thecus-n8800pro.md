Title: Debian on Thecus N8800Pro
Date: 2011-04-05 20:07
Author: Daniel Kahn Gillmor (dkg)
Slug: debian-on-thecus-n8800pro

I recently set up debian on a [Thecus
N8800Pro](http://www.thecus.com/product.php?PROD_ID=28). It seems to be
a decent rackmount 2U chassis with 8 3.5" hot-swap SATA drives, dual
gigabit NICs, a standard DB9 RS-232 serial port, one eSATA port, a bunch
of USB 2.0 connectors, and dual power supplies. I'm happy to be able to
report that Thecus appears to be attempting to fulfill their obligations
under the GPL with this model (i'm fetching [their latest source
tarball](http://www.thecus.com/Downloads/GPL/n4200series_n7700pseries_n8800pseries_5.00.04_GPL.tar.bz2)
now to have a look).

Internally, it's an Intel Core 2 Duo processor, two DIMMS of DDR RAM,
and a PCIe slot for extensions if you like. It has two
internally-attached 128MiB SSDs that it wants to boot off of.

The downsides as i see them:

BIOS

:   The BIOS only runs over VGA, which is not easy to access. I wish it
    ran over the serial port. It's also not clear how one would find an
    upgrade to the BIOS. `dmidecode` reports the BIOS as:

                Vendor: Phoenix Technologies, LTD        Version: 6.00 PG        Release Date: 02/24/2009

RAM
:   The mainboard has only two DIMM sockets. It came pre-populated with
    two 2GiB DDR DIMMs. memtest86+ reports those DIMMs clearly, but sees
    only 3070MiB in aggregate. The linux kernel also sees \~3GiB of RAM,
    despite dmidecode and lshw reporting that the two DIMM modules each
    are only 1GiB in size. I'm assuming this is a bios problem. Anyone
    know how to get access to the full 4GiB? Should i be reporting bugs
    on any of these packages?

SSDs
:   The internal SSDs are absurdly small and very slow for both reading
    and writing. This isn't a big deal because i'm basically only using
    them for the bootloader, kernel, and initramfs. But i'm surprised
    that they could even find 128MiB devices in these days of \$10 1GiB
    flash units.

processor
:   `/proc/cpuinfo` reports two cores of a
    `Intel(R) Core(TM)2 Duo CPU T5500 @ 1.66GHz`, each with \~3333
    bogomips, and apparently without [hardware virtualization
    support](http://www.linux-kvm.org/page/FAQ#How_can_I_tell_if_I_have_Intel_VT_or_AMD-V.3F).
    It's not clear to me whether the lack of virtualization support is
    something that could be fixed with a BIOS upgrade.

I used [Andy Millar's nice
description](http://www.andymillar.co.uk/blog/2010/04/11/installing-linux-on-a-thecus-n8800pro/)
of how to get access to the BIOS -- I found i didn't need a hacksaw to
modify the VGA cable i had. just pliers to remove the outer metal shield
on the side of the connector i plugged into the open socket.

`lshw` sees four `SiI 3132 Serial ATA Raid II Controller` PCI devices,
each of which appears to support two SATA ports. I'm currently using
only four of the 8 SATA bays, so i've tried to distribute the disks so
that each of them is attached to an independent PCI device (instead of
having two disks on one PCI device, and another PCI device idle). I
don't know if this makes a difference, though, in the RAID10
configuration i'm using.

There's also a neat 2-line character-based LCD display on the front
panel which apparently [can be driven by talking to a serial
port](http://thecususergroup.proboards.com/index.cgi?board=n5500modifications&action=display&thread=4089),
though i haven't tried. Apparently there are also LEDs that might be
controllable directly from the kernel via ICH7 and GPIO, but i haven't
tried that yet either.

Any suggestions on how to think about or proceed with a BIOS upgrade to
get access to the full 4GiB of RAM, BIOS-over-serial, and/or enable
hardware virtualization?

**Tags**: [n8800pro](https://debian-administration.org/tag/n8800pro),
[thecus](https://debian-administration.org/tag/thecus)

</p>

