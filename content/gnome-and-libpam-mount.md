Title: GNOME and libpam-mount
Date: 2008-05-10 00:49
Author: Daniel Kahn Gillmor (dkg)
Slug: gnome-and-libpam-mount

So i've been struggling with an Ubuntu 8.04 networked workstation. It
uses [`libpam-mount`](http://packages.qa.debian.org/libpam-mount) to
mount the user's homedir (actually, the mountpoint is one level up from
the homedir) automatically at login over CIFS.

One of the problems i ran into with this arrangement happened because i
was following the `pam_mount` configuration instructions too literally.
In particular, `README.Debian.gz` says:

> For every application used for logging in, there is a file of the form
> `/etc/pam.d/xyz`, add the following line at the end of the file:  
> `@include common-pammount`

In particular, ubuntu's `/etc/pam.d/gdm` defaults to:

    #%PAM-1.0                                                                                                                                                               auth    requisite       pam_nologin.soauth    required        pam_env.so readenv=1auth    required        pam_env.so readenv=1 envfile=/etc/default/locale@include common-authauth    optional        pam_gnome_keyring.so@include common-accountsession required        pam_limits.so@include common-sessionsession optional        pam_gnome_keyring.so auto_start@include common-password

When i added the `@include common-pammount` directive to the bottom of
this file when using pam\_mount, new GNOME sessions failed badly: the
gnome-panel didn't appear (which means that the user couldn't log out
conveniently), and two error messages popped up at each login with nasty
details like:

> No database available to save your configuration: Unable to store a
> value at \[...\], as the configuration server has no writable
> databases.

The problem seems to be that `libpam-gnome-keyring` actually kicks off
`gconfd-2` during its PAM session invocation. If that comes before
`libpam-mount`'s PAM session invocation, then the home directory isn't
mounted for the keyring, and `gconfd-2` decides that it is unable to
save any settings. Since `gconfd` then persists for the rest of the
session, further GNOME session components try to talk to it and it
refuses, even though the gconf db is now available (via the mounted
homedir).

Since the order of the lines in a `/etc/pam.d/*` are semantically
relevant, i'm usually very reluctant to tamper with the defaults.
However, i think the correct `/etc/pam.d/gdm` for this scenario (or any
`pam-mount` scenario using GNOME where the homedir might not be present
at all before the session) is actually:

    #%PAM-1.0                                                                                                                                                               auth    requisite       pam_nologin.soauth    required        pam_env.so readenv=1auth    required        pam_env.so readenv=1 envfile=/etc/default/locale@include common-auth@include common-accountsession required        pam_limits.so@include common-session@include common-password@include common-pammountauth    optional        pam_gnome_keyring.sosession optional        pam_gnome_keyring.so auto_start

With this configuration in place, i can successfully log in with a test
user, anyway (and move on to the next problem, which appears to be
SQLite over CIFS, ugh).

These sorts of problems are tough to nail down:

-   Is this overall problem due to a bug in the documentation for
    `libpam-mount`?
-   In `gdm` for its default weirdly-ordered PAM config?
-   In `libpam-gnome-keyring` for launching `gconf-d-2` in the first
    place?
-   In `libgconf2-4` for `gconfd-2` not being able to notice when the
    directories it wants become available?

Or maybe it's just a configuration detail that i should have known about
and expected to deal with in the first place. Ah, well.

**Tags**: [gnome](https://debian-administration.org/tag/gnome),
[pam](https://debian-administration.org/tag/pam)

</p>

