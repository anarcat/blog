Title: NYC SYEP still requires Microsoft Software
Date: 2010-05-19 19:44
Author: Daniel Kahn Gillmor (dkg)
Slug: nyc-syep-still-requires-microsoft-software

[A year ago, i wrote
about](https://debian-administration.org/users/dkg/weblog/47) how [New
York City's Summer Youth Employment Program
(SYEP)](https://application.nycsyep.com/) requires the use of Internet
Explorer to apply online (and it even appears to require IE just to
download the PDF of the application!)

Sadly, the situation has not changed, a year later. Today, I'm writing
to [Dan
Garodnick](mailto:garodnick@council.nyc.gov?subject=Summer%20Youth%20Employment%20Program%20only%20accessible%20in%20Internet%20Explorer),
Chair of [The City Council's Committee on
Technology](http://legistar.council.nyc.gov/DepartmentDetail.aspx?ID=8866&GUID=72EEA817-E6FF-4051-B853-BC1DAD04E8CC&Search=)
(and the rest of the committee members), [Carole
Post](mailto:cpost@doitt.nyc.gov?subject=Summer%20Youth%20Employment%20Program%20only%20accessible%20in%20Internet%20Explorer),
Commissioner of [DoITT](http://nyc.gov/html/doitt) (the city's
Department of Information Technology and Telecommunications), and
[Jeanne B.
Mullgrav](mailto:jmullgrav@dycd.nyc.gov?subject=Summer%20Youth%20Employment%20Program%20only%20accessible%20in%20Internet%20Explorer),
Commissioner of [DYCD](http://nyc.gov/html/dycd) (the Department of
Youth and Community Development, which runs SYEP).

Here's what i wrote:

    For the last two years at least, the DYCD's Summer Youth Employment Program(SYEP) has been only available to users of Internet Explorer: https://application.nycsyep.com/Internet Explorer (IE) is only made by Microsoft, and is only available forpeople running Microsoft operating systems.  Users of other operating systems,such as GNU/Linux, Macintosh, or others cannot access the SYEP applicationprocess.  Even users of Windows who care about their online security or simplydesire a different web browsing experience might prefer to avoid InternetExplorer.Not only is the online form inaccessible from browsers other than IE, evenretrieving a copy of the PDF to print out and fill in manually is unavailablefor web browsers other than IE.What is the city's policy is on access to government sites?  Is it city policyto mandate a single vendor's software for access to city resources?  Should NYCyouth be required to purchase software from Microsoft to be able to applyfor the Summer Youth Employment Program?The sort of data collection needed by such an application is a mainstay of thestandards-based web, and has been so for over 15 years now.  There is no reasonto require particular client on an open platform.  I can point you towardresources who would be happy to help you make the system functional for usersof *any* web browser, if you like.I raised this issue over a year ago (see nyc.gov correspondence #1-1-473378926,and a public weblog posted around the same time [0]), and got no effectiveremedy.  It's worrisome to see that this is still a problem.Please let me know what you plan to do to address the situation.Regards,  --dkg[0] https://www.debian-administration.org/users/dkg/weblog/47

Feel free to send your own message to the folks above (especially helps
if you live in or near NYC)

Finally, Carole Post, the head of DoITT will also be [present at a panel
tonight in
Soho](http://personaldemocracy.com/user-blog/pdf-evening-series-what-our-cities-are-telling-us-may-19th),
which i'm unfortunately be unable to attend. If you go there, you might
ask her about the situation.

**Tags**: [policy](https://debian-administration.org/tag/policy)

</p>

