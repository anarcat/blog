Title: wireless hardware switches (pciehp is your friend)
Date: 2008-10-16 17:43
Author: Daniel Kahn Gillmor (dkg)
Slug: wireless-hardware-switches-pciehp-is-your-friend

I've been playing around with an eeePC 900, which is [very
well-supported by debian](http://wiki.debian.org/DebianEeePC). Kudos to
the eeepc team!

I had one problem with it, after upgrading
[`eeepc-acpi-scripts`](http://packages.qa.debian.org/eeepc-acpi-scripts)
from version 1.0.4 to 1.0.9: with 1.0.4, i was able to use an ACPI
hotkey to disable and re-enable the wireless. With 1.0.9, the device did
not come back up for me after a toggle. The problem was resolved for me
with:

    echo pciehp >> /etc/modules

which i figured out from reading [a brief
post](http://lists.alioth.debian.org/pipermail/debian-eeepc-devel/2008-October/001300.html)
on the very-informative [`debian-eeepc-devel` mailing
list](http://lists.alioth.debian.org/mailman/listinfo/debian-eeepc-devel).
The rest of this post explores why that was the answer for me.

This entry has been truncated [read the full
entry](https://debian-administration.org/users/dkg/weblog/37).

**Tags**: [eeepc](https://debian-administration.org/tag/eeepc),
[wlan](https://debian-administration.org/tag/wlan)

</p>

