Title: New York State Electronic Records Report
Date: 2008-05-22 07:26
Author: Daniel Kahn Gillmor (dkg)
Slug: new-york-state-electronic-records-report

The CIO and [the Office For Technology of New York State
(USA)](http://www.oft.state.ny.us/) just released a new [report on
electronic records strategy, titled <cite>A Strategy for Openness:
Enhancing E-Records Access in New York
State</cite>](http://www.oft.state.ny.us/policy/esra/erecords-study.htm).

I submitted [my own
comments](http://cmrg.fifthhorseman.net/wiki/NYSRFPC122807) during the
call for public comments earlier this year -- apparently there were only
114 comments submitted by the public (you can see [the call for comments
and an overview/analysis of the
responses](http://www.oft.state.ny.us/policy/esra/erecords/PartIIerecordsStudy.pdf)).
I wish there'd been more, since it looks like the 59 [individual
commenters](http://www.oft.state.ny.us/policy/esra/erecords/PartIII-BerecordsStudy.pdf)
were *overwhelmingly* supportive of a requirement for the Gov't to use
ODF. The rest of the comments were from
[governments](http://www.oft.state.ny.us/policy/esra/erecords/PartIII-CerecordsStudy.pdf),
[NGOs](http://www.oft.state.ny.us/policy/esra/erecords/PartIII-DerecordsStudy.pdf),
and
[businesses](http://www.oft.state.ny.us/policy/esra/erecords/PartIII-EerecordsStudy.pdf),
and they don't seem as unanimous as the individual commenters.

Looking at the metadata for the [executive
summary](http://www.oft.state.ny.us/policy/esra/erecords/PartIerecordsStudy.pdf)
shows just how far we have to go:

``` {.terminal}
[0 dkg@squeak doc]$ pdfinfo PartIerecordsStudy.pdf  | egrep '^Title|Producer|Creator'Title:          Microsoft Word - PartIerecordsStudy.docCreator:        PScript5.dll Version 5.2.2Producer:       Acrobat Distiller 8.1.0 (Windows)[0 dkg@squeak doc]$ 
```

And in a message sent to commenters announcing the report, they seem to
be aware that they're not quite doing their best at interoperability:

> (Please note this is the first time any of NYS government's agencies
> has ever published a document in ODF format. We ran into problems this
> morning not caused by the format, but rather with some controls on our
> systems which were not prepared to encounter such documents. So the
> website links to ODF versions are not working at the moment, but will
> be repaired soon).

What web server are they running that is having trouble "encountering"
ODF? No surprise:

``` {.terminal}
[0 dkg@squeak doc]$ wget -S -O/dev/null 'http://www.oft.state.ny.us/policy/esra/erecords-study.htm' 2>&1 | grep Server  Server: Microsoft-IIS/6.0[0 dkg@squeak doc]$ 
```

Sigh. My initial reaction to the report (i've only read the [executive
summary](http://www.oft.state.ny.us/policy/esra/erecords/PartIerecordsStudy.pdf),
not the [supporting
documentation](http://www.oft.state.ny.us/policy/esra/erecords/PartIIerecordsStudy.pdf))
is disappointment. While they claim that "openness" is an important
feature, they fall far short of taking a strong stand for free and open
formats. The main thrust of the executive summary seems to be
(paraphrasing here, i welcome corrections):

-   it's a bad idea to mandate a single particular technology because
    technologies will change faster than law.
-   we need an Electronic Records Committee (ERC) to provide regular,
    executive guidance on electronic record storage and maintenance
-   "openness" is good, but needs to be weighed against (ill-specified,
    at least in the executive summary) "other features"
-   we're not going to make any concrete recommendations about what to
    do next

While these are unsurprising conclusions, they don't take the strong
stand i'd like them to take. They seem aware of the "chicken and egg"
problem of wide adoption and vendor support, and aware of the
anti-democratic effects of proprietary formats. But they don't seem to
be willing to act as a catalyst to push free formats explicitly. If they
did, they could break the chicken/egg vendor lock-in cycle and
facilitate better open government practices while simultaneously
encouraging a healthy information ecosystem.

At this point, i suppose my biggest hope from the process is that the
proposed ERC forms pilot groups with long-term goals modeled after
[Munich](http://www.muenchen.de/Rathaus/dir/limux/english/147197/), but
i'm not holding my breath. If there are any advocates or participants in
the Munich process who want to share what's been working for you (and
what hasn't), i'd be interested in hearing about it.

**Tags**:
[government](https://debian-administration.org/tag/government),
[odf](https://debian-administration.org/tag/odf),
[policy](https://debian-administration.org/tag/policy)

</p>

