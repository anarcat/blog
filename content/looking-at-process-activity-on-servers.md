Title: looking at process activity on servers
Date: 2006-05-12 17:46
Author: Daniel Kahn Gillmor (dkg)
Slug: looking-at-process-activity-on-servers

The following is just a simple combination of tools that is probably
fairly unremarkable. However, i hope some people will find it useful.

`top` is good at what it does (showing active processes), but the way
the processes jump around in the listings and the way that the process
lineage is displayed makes it less useful for some purposes. For
example, it is difficult to catch relatively short-lived, respawning
processes and understand where they're coming from.

(of course, `top` is insanely featureful (`man top | wc -l` is `1073`!),
so maybe a `top` guru can come along and explain how to make it behave
better for these purposes.)

In the meantime, though, i wanted a command that gives a system-wide
overview of all processes and their lineage, and makes it easy to spot
things which are rapidly respawning on an otherwise stable system. I
want it to work without X11 on the monitored machine, and i'd prefer to
avoid opening up any additional network services where possible. I came
up with this (which can be run over an ssh connection):

    watch -d pstree -Apn

The only drawback is that `watch` will only show you the top screenful
of output, which is not good if your monitor is modestly-sized and the
machine you are monitoring is under fairly heavy load.

A simple workaround for this if you are working from an X11-based
desktop is simply to resize your terminal to be much larger than the
viewport of your monitor. Most X11 setups will let you alt-drag on a
window to move it around so you can see the regions that would otherwise
be cut off. i use `rxvt` for my terminal, so an example would be:

    rxvt -geometry 200x1000 -e ssh -t machine.to.monitor watch -d pstree -Apn

This is why i love unix. Each tool does its part of the job, and i can
put them together to do exactly what i want.

</p>

