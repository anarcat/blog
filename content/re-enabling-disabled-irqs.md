Title: Re-enabling Disabled IRQs?
Date: 2007-12-13 16:40
Author: Daniel Kahn Gillmor (dkg)
Slug: re-enabling-disabled-irqs

I'm seeing more instances with hardware recently where the linux kernel
gets an interrupt that it doesn't like or recognize, and it disables the
IRQ entirely. Is there a way to get that IRQ re-enabled without
restarting? My web searches haven't turned up anything fruitful.

To be clear, the error messages from the kernel look like this:

    irq 9: nobody cared (try booting with the "irqpoll" option) [<c01402e3>] __report_bad_irq+0x2b/0x69 [<c01404d0>] note_interrupt+0x1af/0x1e7 [<c01d60b5>] acpi_irq+0xb/0x14 [<c013fae7>] handle_IRQ_event+0x23/0x49 [<c013fbc0>] __do_IRQ+0xb3/0xe8 [<c01050e5>] do_IRQ+0x43/0x52 [<c01036b6>] common_interrupt+0x1a/0x20 [<c012182f>] __do_softirq+0x51/0xbb [<c01218cf>] do_softirq+0x36/0x3a [<c01050ea>] do_IRQ+0x48/0x52 [<c01036b6>] common_interrupt+0x1a/0x20 [<c0101a5a>] default_idle+0x0/0x59 [<c0101a8b>] default_idle+0x31/0x59 [<c0101b52>] cpu_idle+0x9f/0xb9 [<c03176fd>] start_kernel+0x379/0x380handlers:[<c01d60aa>] (acpi_irq+0x0/0x14)Disabling IRQ #9

booting with `irqpoll` has its own problems (on at least one machine
i've tried that kernel parameter on, it resulted in unrecoverable
lockups -- even ACPI signals didn't get through), so i'd rather not go
there. But it would be nice to be able to get the IRQ back without a
reboot, especially if it's shared across multiple devices.

Any suggestions?

**Tags**: [irq](https://debian-administration.org/tag/irq)

</p>

