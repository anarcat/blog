Title: January 2010 Bug-Squashing Party NYC
Date: 2009-12-21 20:54
Author: Daniel Kahn Gillmor (dkg)
Slug: january-2010-bug-squashing-party-nyc

We're going to have a [Bug-Squashing Party at the end of January 2010 in
New York City](http://wiki.debian.org/BSP2010/NewYorkCity). If you live
in or around the tri-state area (or want to visit), are interested in
learning about the process, meeting other debian folk, or just squashing
some bugs in good company, you should come out and join us!

Where:
:   Brooklyn, New York, USA

When:
:   January 29th, 30th, and maybe 31st of 2010

Why:
:   Because them bugs need squashing!

If you plan on coming, please either sign up on [the wiki
page](http://wiki.debian.org/BSP2010/NewYorkCity), or at least mail one
of the good folks listed there, or pop into [`#debian-nyc` on
`irc.oftc.net`'s IRC network](irc://irc.oftc.net/#debian-nyc).

**Tags**: [bsp](https://debian-administration.org/tag/bsp)

</p>

