Title: xen etch to lenny upgrade serial console
Date: 2009-09-13 23:33
Author: Daniel Kahn Gillmor (dkg)
Slug: xen-etch-to-lenny-upgrade-serial-console

I maintain several xen machines. Most servers that i maintain use serial
consoles (i should probably write another post in more detail about why
serial consoles are the One True Way to manage a server).

Trouble is, the way that xen works with the serial console has changed
between etch and lenny. So what's changed? From what i can tell:

-   The way that the dom0 Linux kernel interacts with the hypervisor's
    console has changed between Linux 2.6.18 (etch) and 2.6.26 (lenny),
    and
-   the Xen hypervisor's method of specifying the console itself has
    changed between Xen 3.0.3 (etch) and 3.2.1 (lenny)

In etch systems, i had a standard GRUB specification like this (as
[noted
earlier](https://www.debian-administration.org/users/dkg/weblog/17)):

    title           Xen 3.0.3-1-i386-pae / Debian GNU/Linux, kernel 2.6.18-4-xen-686root            (hd0,0)kernel          /xen-3.0.3-1-i386-pae.gz dom0_mem=131072 com1=115200,8n1module          /vmlinuz-2.6.18-4-xen-686 root=/dev/mapper/vg_monkey0-dom0 ro console=ttyS0,115200n8 module          /initrd.img-2.6.18-4-xen-686savedefault

but in lenny, i find it necessary to do this:

    title           Xen 3.2-1-i386 / Debian GNU/Linux, kernel 2.6.26-2-xen-686root            (hd0,0)kernel          /xen-3.2-1-i386.gz dom0_mem=131072 com1=115200,8n1 console=com1module          /vmlinuz-2.6.26-2-xen-686 root=/dev/mapper/vg_monkey0-dom0 ro console=hvc0module          /initrd.img-2.6.26-2-xen-686

In particular, the hypervisor itself needs an additional `console=com1`
argument to make sure that it uses the serial console (i suppose the
`com1=...` argument simply specifies *how* to use the serial console
should you need it, instead of a requirement *to* use the console).

And the Linux kernel itself for the dom0 needs to adopt `hvc0` as its
`console`, which i believe is an acronym for something like the
"hypervisor virtual console" -- this way, the dom0 kernel will come out
multiplexed over the hypervisor's console, no matter where that
hypervisor's console is directed (you might not even need to specify
this explicitly -- it might be set up by default).

When i tried to use the etch settings for the console with the lenny
hypervisor and kernel, i saw the hypervisor try to come out on the video
card, and a dom0 crash!

**Tags**: [serial](https://debian-administration.org/tag/serial),
[serial
console](https://debian-administration.org/tag/serial%20console),
[xen](https://debian-administration.org/tag/xen)

</p>

