Title: Compromising webapps: a case study
Date: 2012-03-16 01:42
Author: Daniel Kahn Gillmor (dkg)
Slug: compromising-webapps-a-case-study

[This paper](https://jhalderm.com/pub/papers/dcvoting-fc12.pdf) should
be required reading for anyone developing, deploying, or administering
web applications.

It's also interesting to read [the perspective of the folks operating
the compromised
webapp](http://www.dcboee.org/popup.asp?url=/pdf_files/nr_687.pdf)
(details are in the section titled "Digital Vote-By-Mail" on pages 34 to
38).
