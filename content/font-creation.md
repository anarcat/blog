Title: Font creation
Date: 2005-09-28 21:23
Author: Daniel Kahn Gillmor (dkg)
Slug: font-creation

i just used a set of tools from debian to create a new font based on my
handwriting. i'm providing the font as a ttf file, and as a .deb
package.

links and more details can be found [on my
blog](http://noesis.fifthhorseman.net/node/54).

**Tags**: [font](https://debian-administration.org/tag/font)

</p>

