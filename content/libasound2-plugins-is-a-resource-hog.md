Title: libasound2-plugins is a resource hog!
Date: 2012-12-21 01:50
Author: Daniel Kahn Gillmor (dkg)
Slug: libasound2-plugins-is-a-resource-hog

I run [mpd](http://packages.debian.org/mpd) on debian on "igor", an
NSLU2 -- a very low-power \~266MHz armel machine, with no FPU and a
scanty 32MiB of RAM. This serves nicely to feed my stereo with music
that is controllable from anywhere on my LAN. When playing music and
talking to a single mpd client, the machine is about 50% idle.

However, during a recent upgrade, something wanted to pull in
[pulseaudio](http://packages.debian.org/pulseaudio), which in turn
wanted to pull in
[libasound2-plugins](http://packages.debian.org/libasound2-plugins), and
i distractedly (foolishly) let it. With that package installed, after an
mpd restart, the CPU was completely thrashed (100% utilization) and
music only played in stutters of 1 second interrupted by a couple
seconds of silence. igor was unusable for its intended purpose.

Getting rid of pulseaudio was my first attempt to fix the stuttering,
but the problem remained even after pulse was all gone and mpd was
restarted.

Then i did a little search of which packages had been freshly installed
in the recent run:

    grep ' install .* <none> ' /var/log/dpkg.log

and used that to pick out the offending package.

After purging libasound2-plugins and restarting mpd, the igor is back in
action.

Lesson learned: on low-overhead machines, don't allow apt to install
recommends!

    echo 'APT::Install-Recommends "0";' >> /etc/apt/apt.conf

And it should go without saying, but sometimes i get sloppy: i need to
pay closer attention during an "apt-get dist-upgrade"

**Tags**: [alsa](https://debian-administration.org/tag/alsa),
[apt](https://debian-administration.org/tag/apt),
[low-power](https://debian-administration.org/tag/low-power),
[mpd](https://debian-administration.org/tag/mpd)

</p>

