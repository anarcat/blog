Title: debian testing security Release.gpg broken?
Date: 2006-06-30 19:01
Author: Daniel Kahn Gillmor (dkg)
Slug: debian-testing-security-releasegpg-broken

When i did an `apt-get update` on a mixed etch/sid system last night
(and again this morning), i got a couple errors:

    [0 root@squeak ~]# apt-get update ... Get:3 http://security.debian.org testing/updates Release.gpg                    ...Hit http://security.debian.org testing/updates Release                         Err http://security.debian.org testing/updates Release                           Get:4 http://security.debian.org testing/updates Release [24.3kB] ...Hit http://security.debian.org testing/updates/main Sources    Fetched 24.3kB in 3s (7830B/s)                                 Reading package lists... DoneW: GPG error: http://security.debian.org testing/updates Release: Unknown error executing gpgvW: You may want to run apt-get update to correct these problems[0 root@squeak ~]# 

Fetching the files by hand shows the problem. Release.gpg appears to be
empty for the testing security archive at the moment!

    [0 dkg@squeak tmp.rwonk22339]$ wget -q http://security.debian.org/dists/testing/updates/Release.gpg[0 dkg@squeak tmp.rwonk22339]$ wget -q http://security.debian.org/dists/testing/updates/Release[0 dkg@squeak tmp.rwonk22339]$ ls -ltotal 24-rw-r--r-- 1 dkg dkg 24305 2006-06-29 23:38 Release-rw-r--r-- 1 dkg dkg     0 2006-06-29 23:38 Release.gpg[0 dkg@squeak tmp.rwonk22339]$ 

Does anyone know why this is? Is there anything i can do to help sort
things out?

</p>

