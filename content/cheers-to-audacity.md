Title: Cheers to audacity!
Date: 2015-05-08 18:43
Author: Daniel Kahn Gillmor (dkg)
Slug: cheers-to-audacity

When [paultag recently announced a project to try to move debian
infrastructure to
python3](https://lists.debian.org/debian-devel-announce/2015/04/msg00005.html),
my first thought was how large that undertaking would likely be. It
seems like a classic engineering task, full of work and nit-picky
details to get right, useful/necessary in the long-term, painful in the
short-term, and if you manage to pull it off successfully, the best you
can usually hope for is that no one will notice that it was done at all.

I always find that kind of task a little off-putting and difficult to
tackle, but I was happy to see someone driving the project, since it
does need to get done. Debian is potentially also in a position to help
the upstream python community, because we have a pretty good view of
what things are being used, at least within our own ecosystem.

I'm happy to say that i also missed one of the other great benefits of
paultag's audacious proposal, which is how it has engaged people who
already knew about debian but who aren't yet involved. Evidence of this
engagement is already visible on [the py3porters-devel mailing
list](https://lists.alioth.debian.org/mailman/listinfo/py3porters-devel).
But if that wasn't enough, I ran into a friend recently who told me,
"Hey, I found a way to contribute to debian finally!" and pointed me to
the py3-porters project. People want to contribute to the project, and
are looking for ways in.

So cheers to the people who propose audacious projects and make them
inviting to everyone, newcomers included. And cheers to the people who
step up to potentially daunting work, stake out a task, roll up their
sleeves, and pitch in. Even if the py3porters project doesn't move all
of debian's python infrastructure to pyt3 as fast as paultag wants it
to, i think it's already a win for the project as a whole. I am looking
forward to seeing what comes out of it (and it's reminding me i need to
port some of my own python work, too!)

The next time you stumble over something big that needs doing in debian,
even something that might seem impossible, please make it inviting, and
dive in. The rest of the project will grow and improve from the attempt.

**Tags**:
[py3-porters](https://debian-administration.org/tag/py3-porters)

</p>

