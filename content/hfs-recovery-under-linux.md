Title: HFS+ recovery under linux?
Date: 2005-10-20 04:57
Author: Daniel Kahn Gillmor (dkg)
Slug: hfs-recovery-under-linux

I don't only administer debian (or even debian-based) systems,
unfortunately. i'm trying to deal with an HFS+ volume that Mac OSX
(10.3) appears to have trashed. I'd appreciate any suggestions or advice
people might have. It looks like the table of contents (or something
similar) have been scribbled over. The disk is a 250GB disk, with \~80GB
of data on it. mac's Disk Utility and hditool, (i think hditool is the
underlying tool that the GUI Disk Utility uses) both choke on the disk
with the following output:

    ted:~ admin$ sudo diskutil verifyDisk disk1s3Password:Started verify/repair on disk disk1s3 Checking HFS Plus volume.Invalid B-tree node sizeThe volume  needs to be repaired.Volume check failed.Error detected (-9972) while verifying/repairing disk disk1s3 ted:~ admin$ sudo diskutil repairDisk disk1s3Started verify/repair on disk disk1s3 Checking HFS Plus volume.Invalid B-tree node sizeVolume check failed.Error detected (-9972) while verifying/repairing disk disk1s3 ted:~ admin$ 

when i use Apple's fsck\_hfs, i get the following:

    ted:~ admin$ sudo fsck_hfs -d /dev/rdisk1s3** /dev/rdisk1s3        Block 2 is not an MDB or Volume Header ** Checking HFS Plus volume.   Invalid B-tree node size(4, 0)** Volume check failed.volume check failed with error 7         volume type is pure HFS+         primary MDB is at block 0 0x00         alternate MDB is at block 0 0x00         primary VHB is at block 2 0x02         alternate VHB is at block 488134942 0x1d18591e         sector size = 512 0x200         VolumeObject flags = 0x05         total sectors for volume = 488134944 0x1d185920         total sectors for embedded volume = 0 0x00 ted:~ admin$ 

Seeing as i was getting nowhere with apple's own tools, i figured i'd
try out a debian-based system. i had an ubuntu 5.10 disk lying around
with hfsutils and hfsplus installed on it. hpfsck just chokes as well,
though i don't have the output to paste in right now. i'll put it up as
a comment on this post later. does anyone have any experience with
recovering weirdly garbled HFS+ partitions? any pointers would be most
welcome!

------------------------------------------------------------------------

Edit: here's the output from hpfsck under linux:

    [dkg@squeak ~]$ hpfsck -v /dev/sda3 *** Checking Volume Header:hpfsck: hpfsck: Neither Wrapper nor native HFS+ volume header found (Unknown error 4294967295)[dkg@squeak ~]$ 

Any thoughts or pointers? [this
post](http://www.opendarwin.org/pipermail/discuss/2003-January/003924.html)
seems to imply that fsck\_hfs won't work on little-endian architectures
at all, but i imagine that's changed (or will change) with apple's move
to intel. and [these
guys](http://people.freebsd.org/~yar/hfs/fsck_hfs.html) seem to have
done most of the work for the port a couple years ago, so apple would be
foolish to not incorporate it back in. too bad that it's licensed under
the APSL, which is non-DFSG-free...

</p>

