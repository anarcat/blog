Title: RIP Cookiepuss
Date: 2013-09-28 04:28
Author: Daniel Kahn Gillmor (dkg)
Slug: rip-cookiepuss

Yesterday, i said a sad goodbye to an old friend at [ABC No
Rio](http://abcnorio.org). Cookiepuss was a steadfast companion in my
volunteer shifts at the No Rio computer center, a cranky yet gregarious
presence. I met her soon after moving to New York, and have hung out
with her nearly every week for years.

![\[Cookiepuss -- No Dogs No
Masters\]](https://dkg.fifthhorseman.net/personal/cookie.jpg)

She had the run of the building at ABC No Rio, and was friends with all
sorts of people. She was known and loved by punks and fine artists, by
experimental musicians and bike mechanics, computer geeks and
librarians, travelers and homebodies, photographers, screenprinters,
anarchists, community organizers, zinesters, activists, performers, and
weirdos of all stripes.

For years, she received postcards from all over the world, including
several from people who had never even met her in person. In her younger
days, she was a ferocious mouser, and even as she shrank with age and
lost some of her teeth she remained excited about food.

She was an inveterate complainer; a pants-shredder; a cat remarkably
comfortable with dirt; a welcoming presence to newcomers and a friendly
old curmudgeon who never seemed to really hold a grudge even when i had
to do horrible things like help her trim her nails.

After a long life, she died having said her goodbyes, and surrounded by
people who loved her. I couldn't have asked for better, but I miss her
fiercely.
