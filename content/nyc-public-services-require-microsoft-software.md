Title: NYC public services require Microsoft software
Date: 2009-05-04 23:46
Author: Daniel Kahn Gillmor (dkg)
Slug: nyc-public-services-require-microsoft-software

Providing city services only to users of specific proprietary software
is bad public policy.

I just discovered that New York City's [2009 Summer Youth Employment
Program](https://application.nycsyep.com/) requires Internet Explorer in
order to apply online.

Even downloading the pdf version of the application to print out from
the site is impossible under non-IE browsers because [the actual pdf
link](https://application.nycsyep.com/Documents/SYEP_2009_Participant_Application.pdf)
is wrapped in some IE-only javascript. And even if you could download
the PDF directly, any additional "online information to help you select
a SYEP provider" is inaccessible for the same reason.

I just called [311](http://www.nyc.gov/311) and filed an official
complaint against the [NYC Dept. of Youth and Community
Development](http://www.nyc.gov/html/dycd) (DYCD), who sponsor the
program.

I also called the number on the application page (1-800-246-4646), and
spoke with "Karen" from the DYCD, who explained that the site was a New
York City web site, and that it had been created by [DoITT (the
Department of Information Technology and
Telecommunications)](http://www.nyc.gov/html/doitt) at the request of
the DYCD, but that the DYCD didn't program it directly. She seemed to
misunderstand the tech behind the situation, saying "we can't bring it
to a higher level (like Firefox) because then it wouldn't work for
everyone". I was happy that she understood that Firefox was a concern
here, but the point is not to build the site "higher" to Firefox, but to
use standard technology that all browsers can access for a public site.

Karen also seemed to think the situation was acceptable because the city
youth "can always use IE on local public library computers" to access
the site. Note that the applications involve submitting very detailed
information (SSN, health insurance, family income, criminal record,
selective service registration \#, etc), which are things that i would
personally be unwilling to submit over a shared public computer if i had
any other choice. Furthermore, this crappy implementation decision
encourages the [NYPL](http://nypl.org/) to continue to spend limited
resources on proprietary software to an out-of-state monopolist to run
their computer labs, which is money that could be better spent locally
(or even spent on books or something similarly quaint and library-like).

I'm frustrated. This is 2009. The application process for public
services here should not require any proprietary technology, but it uses
it gratuitously. This excludes legitimate citizens, and encourages
Microsoft in its ongoing pursuit of monopoly status. Both of these are
bad things.

I submitted feedback on [the DYCD customer survey web
site](http://www.nyc.gov/html/dycd/html/contact/customer_survey.shtml),
and submitted two 150-word-limited(!) complaints to [Commissioner
Mulgrav of the DYCD](http://www.nyc.gov/html/mail/html/maildycd.html)
and [Commissioner Cosgrave of
DoITT](http://www.nyc.gov/html/mail/html/maildoitt.html).

I'm sure they'd be interested in hearing from other people about this.
Is this kind of proprietary lock-in what we should expect from [a Mayor
who cuts budgets city-wide *except* for
IT](http://www.nytimes.com/2002/05/28/nyregion/the-mouse-as-sacred-cow-mayor-s-cuts-spare-technology.html)?
Where is all that money going? What is the city getting out of it?

**Tags**: [policy](https://debian-administration.org/tag/policy)

</p>

