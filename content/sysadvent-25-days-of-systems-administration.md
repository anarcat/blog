Title: sysadvent -- 25 days of systems administration
Date: 2008-12-31 23:56
Author: Daniel Kahn Gillmor (dkg)
Slug: sysadvent-25-days-of-systems-administration

I just ran across Jordan Sissel's [sysadvent
project](http://sysadvent.blogspot.com/), where he posted one article
about systems administration each day from the 1st of December to the
25th.

Jordan has been a great communicator when we've exchanged mail (he's the
author of [xdotool](http://packages.qa.debian.org/xdotool), which i
maintain for debian). Unsurprisingly, his posts in sysadvent are also
excellent. He has a broad knowledge of what tools are available, clever
insights in how to connect them together, an engaging and clear writing
style, and sharp sense of what really should matter to a systems
administrator. Definitely worth reading!
