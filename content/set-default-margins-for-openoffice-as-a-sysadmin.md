Title: set default margins for OpenOffice as a sysadmin?
Date: 2012-12-06 17:30
Author: Daniel Kahn Gillmor (dkg)
Slug: set-default-margins-for-openoffice-as-a-sysadmin

I'm maintaining a lab of debian squeeze machines that run OpenOffice.org
(i'm considering upgrading to LibreOffice from squeeze-backports). I'd
like to adjust the default page margins for all users of Writer. Most
instructions i've found [suggest ways to do this as a single
user](http://wiki.openoffice.org/wiki/Documentation/FAQ/Writer/FormattingPagesAndDocuments/How_can_I_change_the_default_Page_Style_margins,_so_that_when_I_start_a_new_document_the_margins_will_be_1_inch%3F),
but not how to make the change system-wide. I don't want to ask every
user of these machines to do this (and i also don't want to tamper with
each home directory directly -- that's not something i can maintain
reliably).

Alas, i can find no documentation about how to change the default page
margins system-wide for either Oo.o or LibreOffice. Surely this is
something that can be done without a recompile. What am i missing?

**Tags**:
[configuration](https://debian-administration.org/tag/configuration),
[libreoffice](https://debian-administration.org/tag/libreoffice),
[margins](https://debian-administration.org/tag/margins),
[openoffice.org](https://debian-administration.org/tag/openoffice.org),
[sysadmin](https://debian-administration.org/tag/sysadmin),
[templates](https://debian-administration.org/tag/templates)

</p>

