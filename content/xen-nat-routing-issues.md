Title: xen NAT routing issues
Date: 2007-12-17 19:52
Author: Daniel Kahn Gillmor (dkg)
Slug: xen-nat-routing-issues

I have the unfortunate circumstance of a xen machine with a single
static, public IP address that needs to host multiple virtual servers
internally. I don't want to bridge the main physical NIC to the virtual
hosts, so i've created a bridge over a `dummy` NIC, and am trying to do
`iptables` filtering with NAT to pass specific ports through to specific
virtualized servers. I have one virtual server which should be granted
full outbound 'net connectivity, masqueraded as though it is coming from
the public IP address. But the source address in the packets from that
virtual host aren't being rewritten.

The dom0, `simian`, runs a stock debian etch xen-linux-system. It has
one public-facing ethernet interface, `eth0`. It has one dummy interface
(`dummy0`, using the `dummy.ko` kernel module).
`simian:/etc/xen/xend-config.sxp` contains:

    (network-script 'network-bridge netdev=dummy0')

The Public IP address for `simian` is 1.2.3.4, but it has an IP address
on `dummy0` of 10.10.10.1/24. The domU in question has a virtual
ethernet device bridged with `dummy0` with IP address 10.10.10.2/24
(gateway 10.10.10.1). I want all outbound requests from the domU to pass
(SNAT'ed) through the public IP address on `eth0`. To do this, i've
enabled forwarding and set up SNAT on `simian`:

    echo 1 > /proc/sys/net/ipv4/conf/dummy0/forwardingecho 1 > /proc/sys/net/ipv4/conf/eth0/forwardingiptables -t nat -A POSTROUTING -o eth0 -p tcp --src 10.10.10.2 -j SNAT --to 1.2.3.4

But when i capture traffic with:

    tcpdump -w /tmp/traffic.pcap -i eth0

i see packets heading out on `eth0` with a 10.10.10.2 source IP address.
Needless to say, TCP sessions from these RFC-1918 reserved addresses
never even get an ACK from a public internet server.

To verify, I just tried capturing packets seen by the next hop upstream
(using the same kind of `tcpdump`, but on the next router down the
line., and the captured packets indeed still have the 10.10.10.2 IP
addresses in them, which means that outbound traffic from the domU will
never be reciprocated.

So why isn't the SNAT rule triggering for these packets? What part of
the netfilter documentation should i re-read with a closer eye to
understand the situation? It seems buggy to me now, but i'm also aware
that i've only skimmed the surface of what's possible.

**Tags**: [nat](https://debian-administration.org/tag/nat),
[routing](https://debian-administration.org/tag/routing),
[xen](https://debian-administration.org/tag/xen)

</p>

