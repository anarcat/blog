Title: converting eviews models to gretl?
Date: 2010-12-13 00:17
Author: Daniel Kahn Gillmor (dkg)
Slug: converting-eviews-models-to-gretl

Someone recently pointed me to [some economic/statistical
models](http://gennaro.zezza.it/software/eviews/v6/gl03sim_a.prg) that
were designed for [eviews (a proprietary tool)](http://www.eviews.com/).

They were looking for help converting the model into a format that could
work with [gretl (the Gnu Regression, Econometrics and Time-series
Library)](http://gretl.sourceforge.net/). I'm afraid i'm an economics
dunce, and i have no experience with this stuff. Any pointers?

**Tags**:
[conversion](https://debian-administration.org/tag/conversion),
[economics](https://debian-administration.org/tag/economics),
[gretl](https://debian-administration.org/tag/gretl)

</p>

