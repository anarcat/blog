Title: Talks and tracks at debconf 10
Date: 2010-04-25 22:29
Author: Daniel Kahn Gillmor (dkg)
Slug: talks-and-tracks-at-debconf-10

I'm helping out on the talks committee for debconf 10 this summer in NYC
(so yes, i'm going to be here for it, even though i don't have that
little badge thingy). This is a call for interested folks to let us know
what you want to see at debconf!

Talks
-----

If you haven't already, [submit your proposal for a talk, performance,
debate, panel, BoF session,
etc](https://penta.debconf.org/penta/submission/dc10/)! We know you've
got good ideas, and [the final call for
contributions](http://lists.debian.org/debian-devel-announce/2010/04/msg00014.html)
went out yesterday, due in less than a week. Please propose your event
soon!

Tracks
------

Also, we want to introduce
[Tracks](http://wiki.debconf.org/wiki/DebConf10/TalkGlossary) as a new
idea for debconf this summer. A good track would thematically group a
consecutive set of debconf events (talks, panels, debates, performances,
etc) to encourage a better understanding of a broader theme. For this to
work, we need a few good people to act as track coordinators for the
areas where they are knowledgeable and engaged.

A track coordinator would have a chance to set the tone and scope for
their track, schedule events, assemble panels or debates, introduce
speakers, and report back at the end of debconf to the larger gathering.
We also hope that a coordinator could identify potential good work being
done in their area, encourage people to submit relevant events for
debconf, and shepherd proposals in their track through the submission
process.

Are you interested in coordinating a track on some topic? Or do you have
a suggestion for someone else who might do a good job on a topic you
want to see well-represented at debconf? You can contact the talk
committee privately if you have questions at <talks@debconf.org>, or you
can contact the whole team publicly at
[debconf-team@lists.debconf.org](https://debian-administration.org/cgi-bin/mailto=).

Some ideas about possible tracks:

-   Science and Mathematics in Debian
-   Debian Integration into the Enterprise
-   Media and Arts and Debian
-   Trends and Tools in Debian Packaging
-   Debian Systems and Infrastructure
-   Debian Community Outreach
-   *...your topic here...*

We can't guarantee that any particular track *will* happen at dc10, but
we can guarantee that it *won't* happen if no one proposes it or
wrangles the relevant events together. Help us make this the best
debconf ever and make sure that your own topical itch gets scratched!

**Tags**: [debconf](https://debian-administration.org/tag/debconf),
[debconf10](https://debian-administration.org/tag/debconf10)

</p>

