Title: OpenPGP User ID Comments considered harmful
Date: 2013-05-15 02:40
Author: Daniel Kahn Gillmor (dkg)
Slug: openpgp-user-id-comments-considered-harmful

Most OpenPGP User IDs look like this:

    Jane Q. Public <jane@example.org>

This is clean, clear, and unambiguous.

However, some tools (`gpg`, `enigmail` among others) ask the user to
provide a "Comment:" field when they are choosing a new User ID (e.g.
when making a new key). ***These UI prompts are evil***. The savvy user
knows to avoid entering anything in this field, so that they can end up
with a User ID like the one above. The user who provides something here
(perhaps even something inconsequential like "I like strawberries", due
to not being sure what should go in this little box) will instead end up
with a User ID like:

    Jane Q. Public (I like strawberries) <jane@example.org>

This is bad. This means that Jane is asking the people who certify her
key+userid to certify whether she actually likes strawberries (how could
they know? what if she changes her mind? should they revoke their
certifications?) and anywhere that she is referred to by name will
include this mention of strawberries. This is not Jane's identity, and
it doesn't belong in [an OpenPGP User ID
packet](https://tools.ietf.org/html/rfc4880#section-5.11).

Furthermore, since User IDs are atomic, if Jane wants to change the
comment field (but leave her name and e-mail address the same), she will
instead need to create a new User ID, publish it, get everyone who has
certified her old key+userid to certify the key+newuserid, and then
revoke the old one.

It is difficult already to help people understand and participate in the
certification network that forms that backbone of OpenPGP's so-called
"web of trust". These bogus comment fields make an already-difficult
task harder. And all because of strawberries!

Tools like `enigmail` and `gpg` ***should not expose the "Comment:"
field to users who are generating keys or choosing new User IDs***. If
they feel it absolutely must be present for some weird corner case that
0.1% of their users will have, they could require that the user enters
some sort of "expert mode" before prompting the user to do something
that is likely to be a mistake.

There is almost no legitimate reason for anyone to use this field. Let's
go through some examples of this people use, taken from some examples i
have lying around (identifying marks have been changed to protect the
innocent who were duped by this bad UI choice, but you can probably find
them on [the public keyserver network](http://pool.sks-keyservers.net/)
if you want to hunt around):

domain repetition

:   John Q. Public (Debian) <johnqpublic@debian.org>

    We know you're with debian already from the `@debian.org` address.
    If this is in contrast to your other address
    (`johnqpublic@example.org`) so that people know where to send you
    debian-related e-mail, this is still not necessary.

    <p>
    Lest you think i'm just calling out debian developers, people with
    `@ubuntu.com` addresses and `(Ubuntu)` comments (as well as
    `@example.edu` addresses and `(Example University)` comments and
    `@example.com` addresses and `(Example Corp)` comments) are out
    there too.

nicknames already evident

:   John Q. Public (Johnny) <johnqpublic@example.net>John Q. Public (wackydude) <wackydude@example.net>

    Again, the information these comments are providing offers no clear
    disambiguation from the info already contained in the name and
    e-mail address, and just muddies the water about what the people who
    certify this identity should actually be trying to verify before
    they make their certification.

"Work"

:   John Q. Public (Work) <johnqpublic@example.com>

    if John's correspondents know that he works for Example Corp, then
    "Work" isn't helpful to them, because they already know this as the
    address that they're writing to him with. If they don't know that,
    then they probably aren't writing to him at work, so they don't need
    this comment either. The same problem appears (for example) with
    literal comments of `(School)` next to their `@example.edu` address.

This is my nth try at this crazy system!

:   John Q. Public (This is my second key) <johnqpublic@example.com>John Q. Public (This is my primary key) <johnqpublic@example.com>John Q. Public (No wait really use this one) <johnqpublic@example.com>

    OpenPGP is confusing, and it can be tricky to get it right. We all
    know :) This is still not part of John's identity. If you want to
    designate a key as your preferred key, keep it up-to-date, get
    people to certify it, and revoke or expire your old keys. People who
    care can look at the timestamps on your keys and tell which ones are
    the most recent ones. You do have a revocation certificate for your
    key handy just in case you lose it, right?

Don't use this key

:   John Q. Public (Old key, do not use) <johnqpublic@example.com>John Q. Public (Please only use this through September 2004) <johnqpublic@example.com>

    This kind of sentiment is better expressed by [revoking the key in
    question](https://tools.ietf.org/html/rfc4880#page-21) or setting an
    expiration time [on the
    key](https://tools.ietf.org/html/rfc4880#section-5.2.3.6) or [User
    ID self-sig](https://tools.ietf.org/html/rfc4880#section-5.2.3.10)
    directly. This sentiment is not part of John's identity, and
    shouldn't be included as though it were.

"none"

:   John Q. Public (none) <johnqpublic@example.com>

    sigh. This is clearly someone getting mixed up by the UI.

I use strong crypto!

:   John Q. Public (3092 bits of RSA) <johnqpublic@example.com>

    This comment refers to the strength of the key material, or the
    algorithms preferred by the user. Since the User ID is associated
    with the key material already, people who care about this
    information can get it from the key directly. This is also not part
    of the user's actual identity.

"no comment"

:   John Q. Public (no comment) <johnqpublic@example.com>

    This is actually not uncommon (some keyservers reply "too many
    matches!"). It shows that the user is witty and can think on their
    feet (at least once), but it is still not part of the user's
    identity.

"offline long-term identity key"

:   This particular comment shows up in alarming numbers on the
    keyservers:

        John Q. Public (offline long-term identity key) <johnqpublic@example.com>

    Again, this is not a part of the user's identity -- it's a statement
    about how the user manages their key. If a third party wants to
    certify this, presumably they need to verify it. How would they
    decide whether this is true or not? It seems like it would make it
    more difficult to get certifications from people who are intent on
    actually verifying the User ID.

    It's also (potentially) wrong: An OpenPGP User ID is bound directly
    to the primary key, but it is implicitly also bound to any subkeys
    that are associated with the primary key. if the subkeys are online,
    but the primary key is offline, is this comment correct? What if the
    primary key later adds a subkey that is *not* offline or long-term
    (e.g. they want to do frequent key rollover for an online subkey)?
    Do they need to revoke their User ID for this?

    <p>
    And where it's not wrong, it's redundant. An OpenPGP certificate is
    by definition a statement about long-term identity that is bound to
    a key in the first place. Why include any of this extra verbiage,
    which is only likely to add to the confusion?

But wait (i hear you say)! I have a special case that actually is a
legitimate use of the comment field that cannot be expressed in OpenPGP
in any other way!

I'm sure that such cases exist. I've even seen one or two of them. The
fact that one or two cases exist does not excuse the fact that that
overwhelming majority of these comments in OpenPGP User IDs are a
mistake, caused only by bad UI design that prompts people to put
something (anything!) in the empty box (or on the command prompt,
depending on your preference).

And this mistake is one of the thousand papercuts that inhibits the
robust growth of the OpenPGP certification network that some people call
the "web of trust". Let's avoid them so we can focus on the other 999
papercuts.

Please don't use comments in your OpenPGP User ID. And if you make a
user interface for OpenPGP that prompts the user to decide on a new User
ID, please *don't* include a prompt for "Comment" unless the user has
already certified that they are really and truly a special special
snowflake.

Thanks!

**UPDATED 2015-02-11:** added "descriptions of key use" example, since
that appears to be more common.

**Tags**: [openpgp](https://debian-administration.org/tag/openpgp),
[ui](https://debian-administration.org/tag/ui)

</p>

