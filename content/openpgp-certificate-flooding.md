Title: OpenPGP Certificate Flooding
Date: 2019-06-28 00:00:00
Author: Daniel Kahn Gillmor (dkg)
Slug: openpgp-certificate-flooding

OpenPGP Certificate Flooding
----------------------------

My public cryptographic identity has been spammed to the point where
it is unusable in standard workflows.  This blogpost talks about what
happened, what I'm doing about it, and what it means for the broader
ecosystem.

If you work with me and you use OpenPGP certificates to do so, the
crucial things you should know are:

 * Do not refresh my OpenPGP certificate from the SKS keyserver
   network.

 * Use a constrained keyserver like
   [keys.openpgp.org](https://keys.openpgp.org) if you want to check
   my certificate for updates like revocation, expiration, or subkey
   rollover.

 * Use an [Autocrypt](https://autocrypt.org)-capable e-mail client,
   [WKD](https://tools.ietf.org/html/draft-koch-openpgp-webkey-service-08),
   or [direct download from my
   server](https://dkg.fifthhorseman.net/dkg-openpgp.key) to find my
   certificate in the first place.
   
 * If you have already fetched my certificate in the last week, and it
   is bloated, or your GnuPG instance is horribly slow as a result,
   you probably want to delete it and then recover it via one of the
   channels described above.

What Happened?
--------------

Some time in the last few weeks, [my OpenPGP certificate,
0xC4BC2DDB38CCE96485EBE9C2F20691179038E5C6](https://dkg.fifthhorseman.net/dkg-openpgp.key)
was flooded with bogus certifications which were uploaded to the [SKS
keyserver network](https://sks-keyservers.net/).

SKS is known to be vulnerable to this kind of [Certificate
Flooding](https://tools.ietf.org/html/draft-dkg-openpgp-abuse-resistant-keystore-03#section-2.1),
and is difficult to address due to the synchronization mechanism of
the SKS pool.  (SKS's synchronization assumes that all keyservers have
the same set of filters). You can see [discussion about this
problem](https://bitbucket.org/skskeyserver/sks-keyserver/issues/57/anyone-can-make-any-pgp-key-unimportable)
from a year ago along with earlier proposals for how to [mitigate
it](https://lists.gnupg.org/pipermail/gnupg-users/2018-January/059799.html).
But none of those proposals have quite come to fruition, and people
are still reliant on the SKS network.

Previous Instances of Certificate Flooding
------------------------------------------

We've seen various forms of certificate flooding before, including
[spam on Werner Koch's
key](https://lists.gnupg.org/pipermail/gnupg-users/2018-January/059753.html)
over a year ago, and abuse tools made available years ago under the
name ["trollwot"](https://github.com/micahflee/trollwot).  There's
even a [keyserver-backed
filesystem](https://github.com/yakamok/keyserver-fs) proposed as a
proof of concept to point out the abuse.

There was even a discussion a few months ago about how [the SKS
keyserver network is
dying](https://lists.riseup.net/www/arc/monkeysphere/2019-04/msg00000.html).

So none of this is a novel or surprising problem.  However, the scale
of spam attached to certificates recently appears to be unprecedented.
It's not just mine: [Robert J, Hansen's certificate has also been
spammed into oblivion](https://dev.gnupg.org/T3972#127338) as
well. The signature spam on Werner's certificate, for example is
"only" about 5K signatures (a total of < 1MiB), whereas the signature
spam attached to mine is more like 55K signatures for a total of
17MiB, and rjh's is more than double that.

What Problems does Certificate Flooding Cause?
----------------------------------------------

The fact that my certificate is flooded quite this badly provides an
opportunity to see what breaks.  I've been filing bug reports and
profiling problems over the last day.

GnuPG [can't even import my certificate](https://dev.gnupg.org/T4591)
from the keyservers any more in the common case.  This also has
implications for ensuring that revocations are discovered, or new
subkeys rotated, as described in that ticket.

In the situations where it's possible to have imported the large
certificate, gpg exhibits [severe performance problems for even basic
operations over the keyring](https://dev.gnupg.org/T4592).

This causes [Enigmail to become unusable](https://dev.gnupg.org/T3972)
if it encounters a flooded certificate.

It also causes problems for [monkeysphere-authentication if it
encounters a flooded certificate](https://bugs.debian.org/931204).

There are probably more!  If you find other problems for tools that
deal with these sort of flooded certs, please report bugs
appropriately.

Dealing with Certificate Flooding
---------------------------------

What can we do about this?  Months ago, i wrote [a draft about
abuse-resistant
keystores](https://tools.ietf.org/html/draft-dkg-openpgp-abuse-resistant-keystore-03)
that outlined these problems and what we need from a keyserver.

### Use Abuse-Resistant Keystores to Refresh Certificates

If the purpose of refreshing your certificate is to find key material
updates and revocations, we need to use an abuse-resistant keyserver
or network of keyservers for that.

Fortunately, [keys.openpgp.org](https://keys.openpgp.org) is just such
a service, and it was recently launched.  It seems to work!  It can
distribute revocations and subkey rollovers automatically, even if you
don't have a user ID for the certificate.  You can do this by putting
the following line in `~/.gnupg/dirmngr.conf` 

    keyserver hkps://keys.openpgp.org

and ensure that there is no `keyserver` entry at all in
`~/.gnupg/gpg.conf`.

This keyserver doesn't distribute third-party certifications at all,
though.  And if the owner of the e-mail address hasn't confirmed with
the operators of `keys.openpgp.org` that they want that keyserver to
distribute their certificate, it [won't even distribute the
certificate's user IDs](https://keys.openpgp.org/about).

### Fix GnuPG to Import certificate updates even without User IDs

Unfortunately, [GnuPG doesn't cope well with importing minimalist
certificates](https://dev.gnupg.org/T4393).  I've applied patches for
this in debian experimental (and they're documented in debian as
[#930665](https://bugs.debian.org/930665)), but those fixes are not yet
adopted upstream, or widely deployed elsewhere.

### In-band Certificate Discovery

Refreshing certificates is only part of the role that keyserver
networks play.  Another is just finding OpenPGP certificates in the
first place.

The best way to find a certificate is if someone just gives it to you
in the context that it makes sense.

The [Autocrypt project](https://autocrypt.org) is an example of this
pattern for e-mail messages.  If you can adopt an Autocrypt-capable
e-mail client, you should, since that will avoid needing to search for
keys at all when dealing with e-mail.  Unfortunately, those
implementations are also not widely available yet.

### Certificate Lookup via WKD or DANE

If you're looking up an OpenPGP certificate by e-mail address, you
should try looking it up via some mechanism where the address owner
(or at least the domain owner) can publish the record.  The current
best examples of this are
[WKD](https://tools.ietf.org/html/draft-koch-openpgp-webkey-service-08)
and [DANE's OPENPGPKEY DNS
records](https://tools.ietf.org/html/rfc7929).  Modern versions of
GnuPG support both of these methods.  See the `auto-key-locate`
documentation in `gpg(1)`.

Conclusion
----------

This is a mess, and it's a mess a long time coming.  The parts of the
OpenPGP ecosystem that rely on the naive assumptions of the SKS
keyserver can no longer be relied on, because people are deliberately
abusing those keyservers.  We need significantly more defensive
programming, and a better set of protocols for thinking about how and
when to retrieve OpenPGP certificates.

A Personal Postscript
---------------------

I've spent a significant amount of time over the years trying to push
the ecosystem into a more responsible posture with respect to OpenPGP
certificates, and have clearly not been as successful at it or as fast
as I wanted to be.  Complex ecosystems can take time to move.

To have my own certificate directly spammed in this way felt
surprisingly personal, as though someone was trying to attack or
punish me, specifically.  I can't know whether that's actually the
case, of course, nor do i really want to.  And the fact that Robert
J. Hansen's certificate was also spammed makes me feel a little less
like a singular or unique target, but I also don't feel particularly
proud of feeling relieved that someone else is also being "punished"
in addition to me.

But this report wouldn't be complete if I didn't mention that I've
felt disheartened and demotivated by this situation.  I'm a stubborn
person, and I'm trying to make the best of the situation by being
constructive about at least documenting the places that are most
severely broken by this.  But I've also found myself tempted to walk
away from this ecosystem entirely because of this incident.  I don't
want to be too dramatic about this, but whoever did this basically
experimented on me (and Robert) directly, and it's a pretty shitty
thing to do.

If you're reading this, and you set this off, and you selected me
specifically because of my role in the OpenPGP ecosystem, or because I
wrote [the abuse-resistant-keystore
draft](https://tools.ietf.org/html/draft-dkg-openpgp-abuse-resistant-keystore),
or because I'm part of the Autocrypt project, then you should know
that I care about making this stuff work for people.  If you'd reached
out to me to describe what you were planning to do, we could have done
all of the above bug reporting and triage using demonstration
certificates, and worked on it together.  I would have happily helped.
I still might!  But because of the way this was done, I'm not feeling
particularly happy right now.  I hope that someone is, somewhere.
