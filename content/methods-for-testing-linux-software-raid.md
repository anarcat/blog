Title: Methods for testing Linux Software RAID?
Date: 2006-07-17 21:31
Author: Daniel Kahn Gillmor (dkg)
Slug: methods-for-testing-linux-software-raid

I have several machines with software RAID (both RAID1 and RAID5
configurations), with fairly modern kernels. I want to test the RAID
before anything bad happens to the machines for real. What methods do
you use to test software RAID on your servers? How do you verify that
the kernel will detect device failures and deal with them properly? I
present some ideas below, but want to hear what other people do for
verification/peace of mind.

This entry has been truncated [read the full
entry](https://debian-administration.org/users/dkg/weblog/6).

**Tags**: [raid](https://debian-administration.org/tag/raid)

</p>

