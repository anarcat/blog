Title: good OCR tools under debian?
Date: 2007-01-26 18:39
Author: Daniel Kahn Gillmor (dkg)
Slug: good-ocr-tools-under-debian

I have never needed to do Optical Character Recognition (turning scanned
documents back into text form), but it appears i may soon need to (in
english, FWIW).

Does anyone have a preferred tool/suite that is packaged for debian?

A scan of the archive turns up

-   `gocr`
-   `ocrad`
-   `unpaper`
-   `clara`

none of which i've ever used, and some of which seem stale (`clara`'s
version number is `20031214-2`. Suggestions? Things to avoid? Have i
missed something important?

</p>

