Title: debian etch updated to 4.0r1
Date: 2007-08-17 01:53
Author: Daniel Kahn Gillmor (dkg)
Slug: debian-etch-updated-to-40r1

It looks like [debian etch just received its first point
release](http://times.debian.net/1161-etch-r1), updating many useful
packages with little nagging details that didn't make it into the first
release.

Aside from the security fixes, one update i'm particularly happy to see
is the fix of the `libneon26` kerberos/GSSAPI authentication mechanism,
which caused me a lot of pain.

What's your favorite update in the batch here? what update do you think
is highest priority for the next point release?

As always, thanks to everyone who does this work.
