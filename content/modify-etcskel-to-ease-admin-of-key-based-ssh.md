Title: modify /etc/skel to ease admin of key-based SSH
Date: 2007-06-13 03:54
Author: Daniel Kahn Gillmor (dkg)
Slug: modify-etcskel-to-ease-admin-of-key-based-ssh

When i set up a new machine these days, I often disable password-based
ssh access by setting `PasswordAuthentication` and
`ChallengeResponseAuthentication` to `no` in `/etc/ssh/sshd_config`.

This protects my users (and the machine) from dictionary attacks based
on the crappy passwords that humans seem to traditionally prefer.
However, `/usr/sbin/sshd` is justifiably picky about whether to trust
the contents of the user's key file, which is (by default) stored in
`~/.ssh/authorized_keys`. In particular, the file must be writable only
by the user or `root`, and the directory it is stored in should have the
same restrictions (so that an altered file can't be moved into place
maliciously).

so i set up the machine to automatically create a blank
`authorized_keys` file for the user upon account creation, with the
appropriate permissions and ownership. This is easy to do:

    mkdir /etc/skel/.sshtouch /etc/skel/.ssh/authorized_keys

Now, when i get a public key i want to use to authorize `fred`, i can
drop it into the appropriate file simply by doing:

    cat >> ~fred/.ssh/authorized_keys

followed by pasting the key into the buffer, and then `ctrl-D` (to
signify end-of-file). `fred` can now log in, and i don't have to worry
about tuning permissions or building out the directory if it's not
already there.

Note: i'm aware that there are problems with key-based authentication as
well, but given that the majority of these boxes are on the public
internet, it's a pretty good protection against the most common class of
attack out there: the brute-force password scanning attack. It also
tends to protect against the "I use a good password, but it's the same
password on every system i contact" class of foible, since a compromised
machine which knows a user's public key for authentication can't use it
to compromise another system which trusts the same pubkey. So at the
moment, it seems to me like the authentication option which sucks less
than all the others.

**Tags**: [public key
authentication](https://debian-administration.org/tag/public%20key%20authentication),
[ssh](https://debian-administration.org/tag/ssh),
[tip](https://debian-administration.org/tag/tip)

</p>

