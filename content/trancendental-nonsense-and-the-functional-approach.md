Title: Trancendental Nonsense and the Functional Approach
Date: 2009-01-22 05:32
Author: Daniel Kahn Gillmor (dkg)
Slug: trancendental-nonsense-and-the-functional-approach

Poking around [the web site for Law in Contemporary
Society](http://emoglen.law.columbia.edu/twiki/bin/view/LawContempSoc/WebHome),
a class taught this semester by [Eben
Moglen](http://emoglen.law.columbia.edu/), (who is counsel for the [Free
Software Foundation](http://fsf.org/) and founder of the [Software
Freedom Law Center](http://softwarefreedom.org)), i found [Felix
Cohen](http://en.wikipedia.org/wiki/Felix_S._Cohen)'s [Trancendental
Nonsense and the Functional
Approach](http://moglen.law.columbia.edu/LCS/cohen-transcendental.pdf),
which (according to wikipedia) is one of "the most-cited law review
articles ever written".

I haven't read the whole thing yet (and i'm neither a lawyer nor a
philosopher) but it's fascinating reading. And from what i've read so
far, it's a strong push toward directly addressing the *values* that lie
hidden beneath our technical or mechanical decisions, and to avoid
mistaking technical success or skill with a worthwhile outcome and clear
goals at a societal level. This is something we software developers and
system administrators struggle with as well (or at least i think we
should). It's neat to get my head around these concepts from a different
intellectual sphere, and a different era (74 years ago!) when the
technical and mechanical tools i work with didn't exist in anything like
their present form.

This kind of reading makes me wonder what works from Computer Science or
Systems Engineering or Information Technology will have this kind of
exhortative power and social relevance so far into the future. Do you
have a favorite (or abhorred?) text from your field that offers the kind
of moral and technical challenges that Cohen's work does?

**Tags**: [law](https://debian-administration.org/tag/law),
[moglen](https://debian-administration.org/tag/moglen),
[philosophy](https://debian-administration.org/tag/philosophy)

</p>

