Title: USB Infrared recommendations for debian?
Date: 2007-12-12 05:08
Author: Daniel Kahn Gillmor (dkg)
Slug: usb-infrared-recommendations-for-debian

I just noticed that one of the bodegas in my neighborhood sells
"universal remotes" for a few USD. These are designed to work with
arbitrary televisions, and can be switched/programmed to emit various IR
signals. I've got an [NSLU2 running
sid](http://www.cyrius.com/debian/nslu2/), which feeds audio to my
stereo using [mpd](http://packages.debian.org/mpd). I'd like to use one
of the cheap-o remotes to at least start/stop the audio and adjust the
volume.

I suspect that the way to do that is with
[LIRC](http://www.lirc.org/faq.html) (as [Utumno
mentioned](https://debian-administration.org/comment/onweblog/880)), but
i'm not sure what kind of USB infrared devices will be compatible. The
NSLU2 only has USB ports -- no RS-232 or anything else. Anyone with more
experience in this area care to suggest a USB IR receiver that works
well with a TV remote?

**Tags**: [ir](https://debian-administration.org/tag/ir),
[mouse](https://debian-administration.org/tag/mouse),
[mpd](https://debian-administration.org/tag/mpd)

</p>

