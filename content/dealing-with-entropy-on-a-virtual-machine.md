Title: dealing with entropy on a virtual machine
Date: 2009-12-12 18:42
Author: Daniel Kahn Gillmor (dkg)
Slug: dealing-with-entropy-on-a-virtual-machine

I've been using virtual machines (KVM, these days) as isolated
environments to do things like build packages as root. Unfortunately,
some of these activities require decent-sized chunks of random data
(pulled from `/dev/random`). But `/dev/random` pulls from the kernel's
entropy pool, which in turn is replenished from "hardware" events. But a
virtual machine has no actual hardware, and if it is only doing isolated
package builds, there is very little activity to feed the kernel's
entropy pool. So the builds and test suites that rely on this randomness
all hang for a long long time. :(

My current way to get around this is to replace `/dev/random` with the
`/dev/urandom` device, which does not block if the entropy pool is
depleted:

    mknod /dev/newrandom c 1 9chmod --reference=/dev/random /dev/newrandommv -f /dev/newrandom /dev/random

This has the consequence that the "randomness" these commands use
doesn't have as much "real" entropy, though some operating systems (like
FreeBSD) have a non-blocking `/dev/random` by default (and it's also
questionable what "real" entropy means for a virtual machine in the
first place).

I'm also using [cowbuilder](http://packages.debian.org/cowbuilder)
within these VMs to do package builds. But cowbuilder has its own `/dev`
tree, with its own device nodes, so this needs to be fixed too. So after
you have successfully done `cowbuilder --create`, you need to modify the
`random` device within the cowbuilder chroot:

    mknod /var/cache/pbuilder/base.cow/dev/newrandom c 1 9chmod --reference=/var/cache/pbuilder/base.cow/dev/random /var/cache/pbuilder/base.cow/dev/newrandommv -f /var/cache/pbuilder/base.cow/dev/newrandom /var/cache/pbuilder/base.cow/dev/random

Hopefully this will be useful for other people using cowbuilder (or
other build strategies) on isolated virtual machines. If you've worked
around this problem in other ways (or if there's a security concern
about this approach), i'd be happy to hear about the details.

</p>

