Title: hotmail thinks powerpc means mobile
Date: 2010-09-21 19:55
Author: Daniel Kahn Gillmor (dkg)
Slug: hotmail-thinks-powerpc-means-mobile

Apparently, live.com thinks that any browser coming from a ppc
architecture is a mobile device. This sucks for the users of the
hundreds of thousands of powerpc desktops still in service.

I don't use hotmail myself, but i do support people who use it. I set
one of my clients up with debian squeeze on their PPC machine because
all the proprietary vendors have basically given up on that architecture
-- debian represents the best way to get modern tools on these machines
(and other machines too, but that's a different argument).

However, this client couldn't get to their hotmail account, despite
using the latest version of iceweasel (3.5.12). They were directed to a
crippled interface that didn't include the ability to attach files, and
was a gross waste of the desktop screen space. It appears to be the
"mobile" version of live.com's services.

However, the same version of iceweasel on an i686 test machine could
access the standard version of hotmail with no trouble. My friend
jeremyb helpfully suggested fiddling with the User Agent string exported
by the browser. Some experimentation shows that the presence of the
string "ppc" within any parenthetical expression in the UA makes
live.com show the crappy interface. You can try it yourself (if you have
a hotmail account) on your x86 or amd64 machine by adding `(ppc)` to the
default valule of `general.useragent.extra.firefoxComment` in
`about:config`. Stupid stupid stupid.

I'd like to have fixed this by overriding the browser's reported
architecture (or simply by removing it -- why does a web server need to
know the hardware architecture of my client?). But there doesn't appear
to be a way to do that with [the way that mozilla constructs the
UA](http://mxr.mozilla.org/mozilla-central/source/netwerk/protocol/http/nsHttpHandler.cpp#719).
Instead, i needed to add a new string key [named
`general.useragent.override`](http://kb.mozillazine.org/Firefox_:_FAQs_:_About:config_Entries#General.)
which is not exposed by default in `about:config`.

This raises some questions:

-   Why are we publishing our hardware architectures from our browsers
    anyway? This seems like unncessary leakage, and not all browsers do
    it. For example, [Arora](http://code.google.com/p/arora) doesn't
    leak this info (despite [a poorly-argued request to do
    so](http://code.google.com/p/arora/issues/detail?id=876)). [Browsers
    are already too identifiable by
    servers](http://panopticlick.eff.org/). This information should not
    be leaked by default.
-   Why does live.com insist on sending ppc users to the crappy "mobile"
    version? Are they trying to encourage the treadmill of hardware
    upgrades that proprietary vendors benefit from? Is there some less
    insidious explanation? Are there actually more powerpc-based mobile
    devices than desktops?
-   why is there no simple way to tell Firefox/Iceweasel to override or
    suppress the architecture information? Having to override the
    useragent string entirely means that when iceweasel does eventually
    get upgraded, it's going to report the wrong version unless i can
    remember to update the override myself (i can't reasonably expect a
    non-techie client who never heard of user agents before today to
    remember how to do this correctly).

Any ideas?

**Tags**: [browser](https://debian-administration.org/tag/browser),
[hotmail](https://debian-administration.org/tag/hotmail),
[powerpc](https://debian-administration.org/tag/powerpc),
[ppc](https://debian-administration.org/tag/ppc),
[useragent](https://debian-administration.org/tag/useragent),
[wtf](https://debian-administration.org/tag/wtf)

</p>

