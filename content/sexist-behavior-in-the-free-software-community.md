Title: sexist behavior in the free software community
Date: 2009-10-01 20:32
Author: Daniel Kahn Gillmor (dkg)
Slug: sexist-behavior-in-the-free-software-community

So not even 3 months out from [RMS's sexist Gran Canaria “virgins”
remarks](http://opensourcetogo.blogspot.com/2009/07/emailing-richard-stallman.html),
we have another powerful leader in the Free Software Community [making
sexist remarks in a talk to developers (this time, it's Mark
Shuttleworth)](http://blog.linuxtoday.com/blog/2009/09/mark-shuttlewor-1.html).
It's a shame that these two people have said stupid things that hurt
their causes and their communities by perpetuating an unfriendly
environment for women. And it's a bigger shame that neither leader
appears to care enough about their community to issue a sincere public
apology for their screwup (if i'm wrong about this, please point me to
the apology — i've looked).

These guys are in a situation which is nowhere near as hard as writing
good software or managing complex technical projects: if you make a
stupid mistake, own up to it, apologize, and try not to make similar
mistakes in the future.

Perhaps worst of all, are the remarkable number of unreasonably
fucked-up comments on the blog posts discussing these unfortunate
events. If you're in the habit of defending remarks like those made by
RMS and Shuttleworth on the 'net, please take a minute and ask yourself
a few questions:

-   Do you think that the Free Software community today is
    overwhelmingly male (even by the standards of the male-dominated IT
    industry)? If not, thanks for playing. You are living in a fantasy
    world. Try some basic research.
-   Do you think that the significant under-representation of women is a
    problem? Let's say there are about three answers here:

    Gender disparity in Free Software is a Good Thing
    :   If this is your position, please announce it explicitly so we
        all know. Just so you know: I don't want to be part of your
        all-boys club. You can stop these questions now, sorry to have
        bothered you.

    I don't really care about gender disparity in Free Software one way or the other
    :   You may not care; but a significant subset of the Free Software
        community thinks that it's a problem and would like to address
        it. Please keep this in mind as you go to the next question.
        Also, have you thought much about the idea of privilege and how
        it might apply to your situation?

    I think gender disparity in Free Software is probably a Bad Thing
    :   Great, glad we agree on that.

-   People in our community have a problem with the current state of
    affairs, and point out some specific behavior that makes the bad
    situation worse. What should you do?
    <dl>
    <dd>
    <dt>
    Shout them down or attack them
    <dd>
    Gee, it sure is upsetting to hear people talk about problems in the
    community. It's almost as upsetting as getting bug reports about
    problems in our software. Shall we shout them down too? Maybe we
    should attack them! Condescension is also great. Those silly bug
    reporters!
    <dt>
    Argue them out of having a problem
    <dd>
    This just doesn't work very well. Someone has already volunteered to
    tell you about a problem that you hadn't noticed. You are unlikely
    to convince them that they were imagining things.
    <dt>
    Take them seriously
    <dd>
    Yes! It seems to be surprising to some commentators that this is not
    a witch hunt or a lynch mob (interesting that these terms often-used
    in defense of white men connote specific historical traditions of
    the exercise of male privilege and white privilege, respectively).
    Well-meaning people have respectfully raised good-faith concerns
    about the state of our community, and made very simple suggestions
    about what to do to make the community more welcoming to women: lay
    off the sexist remarks at conferences, apologize when some nonsense
    does slip through — we're all struggling with various kinds of
    internalized oppression, you won't be perfect — and try not to do it
    again. Why not listen to these people? Why not support them?
    </dl>

Please read [the Geek Feminism
wiki](http://geekfeminism.wikia.com/wiki/Geek_Feminism_Wiki) and
[blog](http://geekfeminism.org/). Even if you don't agree with
everything on those sites (hey, it's a wiki! and a blog! you don't have
to agree with everything!), people are at least trying to address the
problem of sexism in our community there. Engage constructively and
don't hide or ignore problems!

**UPDATE:** I've asked the site admin to remove some trollish comments.
If you want to use the comments on this blog to announce something like
"i'd like for free software to be a boys-only club", that's fine, i'd
like to know who you are so i can adjust my perception of you
accordingly. Anything advocating or excusing rape and/or murder is way
over the line and is not welcome here. i'm sure you can find some other
place to troll.

**Tags**: [community](https://debian-administration.org/tag/community),
[conduct](https://debian-administration.org/tag/conduct),
[feminism](https://debian-administration.org/tag/feminism), [free
software](https://debian-administration.org/tag/free%20software),
[sexism](https://debian-administration.org/tag/sexism)

</p>

