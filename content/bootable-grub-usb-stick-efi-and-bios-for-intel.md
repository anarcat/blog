Title: Bootable grub USB stick (EFI and BIOS for Intel)
Date: 2015-03-16 23:12
Author: Daniel Kahn Gillmor (dkg)
Slug: bootable-grub-usb-stick-efi-and-bios-for-intel

I'm using [grub](https://www.gnu.org/software/grub/) version
2.02\~beta2-2.

I want to make a USB stick that's capable of booting Intel architecture
EFI machines, both 64-bit (x86\_64) and 32-bit (ia32). I'm starting from
a USB stick which is attached to a running debian system as `/dev/sdX`.
I have nothing that i care about on that USB stick, and **all data on it
will be destroyed by this process**.

I'm also going to try to make it bootable for traditional Intel BIOS
machines, since that seems handy.

I'm documenting what I did here, in case it's useful to other people.

Set up the USB stick's partition table:

    parted /dev/sdX -- mktable gptparted /dev/sdX -- mkpart biosgrub fat32 1MiB 4MiBparted /dev/sdX -- mkpart efi fat32 4MiB -1parted /dev/sdX -- set 1 bios_grub onparted /dev/sdX -- set 2 esp on

After this, my 1GiB USB stick looks like:

``` {.terminal}
0 root@foo:~# parted /dev/sdX -- printModel:  USB FLASH DRIVE (scsi)Disk /dev/sdX: 1032MBSector size (logical/physical): 512B/512BPartition Table: gptDisk Flags: Number  Start   End     Size    File system  Name      Flags 1      1049kB  4194kB  3146kB  fat32        biosgrub  bios_grub 2      4194kB  1031MB  1027MB               efi       boot, esp0 root@foo:~# 
```

make a filesystem and mount it temporarily at `/mnt`:

    mkfs -t vfat -n GRUB /dev/sdX2mount /dev/sdX2 /mnt

ensure we have the binaries needed, and add three grub targets for the
different platforms:

    apt install grub-efi-ia32-bin grub-efi-amd64-bin grub-pc-bin grub2-commongrub-install --removable --no-nvram --no-uefi-secure-boot \     --efi-directory=/mnt --boot-directory=/mnt \     --target=i386-efigrub-install --removable --no-nvram --no-uefi-secure-boot \     --efi-directory=/mnt --boot-directory=/mnt \     --target=x86_64-efigrub-install --removable --boot-directory=/mnt \     --target=i386-pc /dev/sdX

At this point, you should add anything else you want to `/mnt` here! For
example:

-   a kernel and initramfs from your own computer
-   [debian-installer
    ISOs](https://www.debian.org/devel/debian-installer/)
-   firmware blobs for that annoying proprietary device
-   BIOS updates
-   [debirf images](http://debirf.cmrg.net/autobuilds/)
-   [grub-invaders](https://tracker.debian.org/pkg/invaders)
-   ...

And don't forget to cleanup:

    umount /mntsync

**Tags**: [bios](https://debian-administration.org/tag/bios),
[efi](https://debian-administration.org/tag/efi),
[grub](https://debian-administration.org/tag/grub),
[tip](https://debian-administration.org/tag/tip)

</p>

