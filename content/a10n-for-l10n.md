Title: a10n for l10n
Date: 2014-12-12 23:00
Author: Daniel Kahn Gillmor (dkg)
Slug: a10n-for-l10n

The abbreviated title above means "Appreciation for Localization" :)

I wanted to say a word of thanks for the awesome work done by [debian
localization teams](https://wiki.debian.org/L10n). I speak English, and
my other language skills are weak. I'm lucky: most software I use is
written by default in a language that I can already understand.

The debian localization teams do great work in making sure that packages
in debian gets translated into many other languages, so that many more
people around the world can take advantage of free software.

I was reminded of this work recently (again) with the great patches
submitted to [GnuPG and related
packages](https://wiki.debian.org/Teams/GnuPG). The changes were made by
many different people, and coordinated with the debian GnuPG packaging
team by David Prévot.

This work doesn't just help debian and its users. These localizations
make their way back upstream to the original projects, which in turn are
available to many other people.

If you use debian, and you speak a language other than english, and you
want to give back to the community, please consider [joining one of the
localization teams](https://wiki.debian.org/L10n). They are a great way
to help out our project's top [priorities: our users and free
software](https://www.debian.org/social_contract).

Thank you to all the localizers!

(this post was inspired by [gregoa's debian advent
calendar](http://info.comodo.priv.at/blog/archive/2014/12/). i won't be
posting public words of thanks as frequently or as diligently as he
does, any more than i'll be fixing the number of RC bugs that he fixes.
This are just two of the ways that gregoa consistently leads the
community by example. He's an inspiration, even if living up to his
example is a daunting challenge.)
