Title: Insane clock skew on amd64 host running etch/i386
Date: 2007-02-02 02:07
Author: Daniel Kahn Gillmor (dkg)
Slug: insane-clock-skew-on-amd64-host-running-etchi386

My impression was that the i386 debian distribution could run fine on an
amd64 processor (though i recognize that it wouldn't get the full
benefit of the 64-bit platform).

I have a machine which i'm trying to do this on, though, and while it's
up and running, i'm getting serious clock skew: something like 10x real
time. The machine is running stock debian etch, as up-to-date as you can
get it, with a 2.6.18-3-686 kernel.

Interestingly, the hardware clock is stable, and very close to real
time. I think i might just be getting way too many timer interrupts or
something. Any suggestions about what to do? Somehow, i don't think ntp
was designed to handle a skew of 10 seconds per second.

Here's a debugging attempt where you can see the skew:

    0 orangutan:~# hwclock --hctosys; for foo in 1 2 3 4; do>    date +%c>    hwclock --show>    ntpdate -q -u sundial.columbia.edu>    sleep 10> doneThu 01 Feb 2007 09:05:53 PM ESTThu 01 Feb 2007 09:05:54 PM EST  -0.997837 secondsserver 128.59.59.177, stratum 2, offset -2.570964, delay 0.40991 1 Feb 21:05:55 ntpdate[6909]: step time server 128.59.59.177 offset -2.570964 secThu 01 Feb 2007 09:06:05 PM ESTThu 01 Feb 2007 09:05:55 PM EST  -0.256843 secondsserver 128.59.59.177, stratum 2, offset -13.593056, delay 0.41589 1 Feb 21:06:07 ntpdate[6913]: step time server 128.59.59.177 offset -13.593056 secThu 01 Feb 2007 09:06:17 PM ESTThu 01 Feb 2007 09:05:56 PM EST  -0.252818 secondsserver 128.59.59.177, stratum 2, offset -24.668502, delay 0.41322 1 Feb 21:06:19 ntpdate[6917]: step time server 128.59.59.177 offset -24.668502 secThu 01 Feb 2007 09:06:29 PM ESTThu 01 Feb 2007 09:05:57 PM EST  -0.256849 secondsserver 128.59.59.177, stratum 2, offset -35.685214, delay 0.41321 1 Feb 21:06:31 ntpdate[6921]: step time server 128.59.59.177 offset -35.685214 sec0 orangutan:~# 

Any suggestions? What should i be looking for?

**Tags**: [clock](https://debian-administration.org/tag/clock)

</p>

