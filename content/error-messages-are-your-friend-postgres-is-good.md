Title: Error messages are your friend (postgres is good)
Date: 2012-12-04 20:32
Author: Daniel Kahn Gillmor (dkg)
Slug: error-messages-are-your-friend-postgres-is-good

Here is a bit of simple (yet subtly-flawed) sql, which produces
different answers on different database engines:

``` {.terminal}
0 dkg@pip:~$ cat test.sqldrop table if exists foo;create table foo (x int, y int);insert into foo VALUES (1,3);insert into foo VALUES (1,5);select y from foo group by x;0 dkg@pip:~$ sqlite3 < test.sql50 dkg@pip:~$ mysql -N dkg < test.sql30 dkg@pip:~$ psql -qtA dkg < test.sqlERROR:  column "foo.y" must appear in the GROUP BY clause or be used in an aggregate functionLINE 1: select y from foo group by x;               ^0 dkg@pip:~$ 
```

-   Clear error reporting and
-   an insistence on explicit disambiguation

are two of the many reasons postgresql is my database engine of choice.

**Tags**: [errors](https://debian-administration.org/tag/errors),
[postgresql](https://debian-administration.org/tag/postgresql),
[sql](https://debian-administration.org/tag/sql)

</p>

