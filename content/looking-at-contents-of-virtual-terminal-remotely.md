Title: looking at contents of virtual terminal remotely?
Date: 2008-01-10 15:50
Author: Daniel Kahn Gillmor (dkg)
Slug: looking-at-contents-of-virtual-terminal-remotely

There is a machine i have remote superuser access to which i know has
some interesting info on one of the VTs (`/dev/tty2` in particular in
this case). I can try to scare up someone physically on-site to plug in
a monitor, and painfully transcribe the text there by hand, but i'd
prefer the simpler, politer (and less error-prone) option of getting the
data digitally myself via ssh.

Any ideas how i could find the text that is already displayed on that
VT? The machine is running a stripped-down debian etch, but i could add
packages if i need to.

This entry has been truncated [read the full
entry](https://debian-administration.org/users/dkg/weblog/28).

**Tags**:
[screendump](https://debian-administration.org/tag/screendump),
[tip](https://debian-administration.org/tag/tip),
[tips](https://debian-administration.org/tag/tips)

</p>

