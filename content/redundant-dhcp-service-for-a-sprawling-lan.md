Title: redundant DHCP service for a sprawling LAN?
Date: 2009-02-27 19:44
Author: Daniel Kahn Gillmor (dkg)
Slug: redundant-dhcp-service-for-a-sprawling-lan

I'm supporting a medium-sized (\~70 regular clients and a half-dozen
servers) LAN. It's a single logical ethernet segment, but it's built as
a tree of cascaded switches. DHCP service is provided by a single host
running ISC's DHCPD (via
[dhcp3-server](http://packages.debian.org/dhcp3-server)).

DHCP itself is one of the single points of failure in the network
layout. i'd really like to make this DHCP server redundant (so that i
can take that host down for service if needed and leave the rest of the
network intact). However, reading
[dhcpd.conf(5)](http://linux.die.net/man/5/dhcpd.conf) makes me pretty
worried that the failover stuff is not well-tested or widely deployed.

I've read [Paul Heinlein's *Failover with ISC
DHCP*](http://www.madboa.com/geek/dhcp-failover/), which makes it look
not unreasonable, but i was wondering if people have other preferred
mechanisms for providing DHCP redundancy. Do you have failover DHCP set
up for any LAN that you manage? If so, what do you use? Are there any
gotchas to watch out for?

I'm also concerned about the security implications. On a network that's
not using IPSEC, i don't see any mechanism for the two DHCP servers to
properly mutually authenticate. Is it really just by IP address? Could
someone spoofing the IP address of one host corrupt the state of the
other DHCP server? (i'm less concerned about them keeping network
traffic private, since most of what they communicate is likely to go out
in the clear on the wire anyway). Am i missing some clever
authentication technique?

From a security point of view, i understand that there are more severe
security problems with DHCP itself, of course (the protocol requires
that the client trust the (unauthenticated) server), but that doesn't
seem likes a good reason to introduce an opportunity to compromise any
given server directly.

Your thoughts on DHCP redundancy?

**Tags**: [dhcp](https://debian-administration.org/tag/dhcp),
[failover](https://debian-administration.org/tag/failover),
[redundancy](https://debian-administration.org/tag/redundancy)

</p>

