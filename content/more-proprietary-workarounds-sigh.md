Title: more proprietary workarounds, sigh
Date: 2012-11-27 08:39
Author: Daniel Kahn Gillmor (dkg)
Slug: more-proprietary-workarounds-sigh

In supporting a labful of Debian GNU/Linux machines with NFS-mounted
home directories, i find some of my users demand a few proprietary
programs. [Adobe Flash](http://wiki.debian.org/FlashPlayer) is one of
the most demanded, in particular because some popular streaming video
services (like [Amazon Prime](http://amazon.com/prime) and
[Hulu](http://hulu.com)) seem to require it.

I'm not a fan of proprietary network services, but i'm happy to see that
[Amazon Prime takes Linux support seriously
enough](http://www.amazon.com/gp/help/customer/display.html/ref=atv_drm_flash_help?nodeId=200256920#playbackerror)
to direct users to [Adobe's Linux Flash "Protected Content"
troubleshooting
page](http://helpx.adobe.com/x-productkb/multi/flash-player-11-problems-playing.html)
(Amazon Prime's rival NetFlix, by comparison, has an abysmal record on
this platform). Of course, none of this will work on any platform but
i386, since the flash player is proprietary software and its proprietors
have shown no interest in porting it or letting others port it :(

One of the main issues with proprietary network services is their
inclination to view their customer as their adversary, as evidenced by
various DRM schemes. In two examples, the Flash Player's DRM module
[appears to arbitrarily break when you use one home directory across
multiple machines](http://forums.adobe.com/thread/1105464). Also, the
DRM module appears to [depend on
HAL](http://forums.adobe.com/message/4594136), which is being deprecated
by most of the common distributions.

Why bother with this kind of gratuitous breakage? We know that video
streaming can and does work fine without DRM. With modern browsers,
freely-formatted video, and HTML5 video tags, video just works, and it
works under the control of the user, on any platform. But Flash appears
to throw up unnecessary hurdles, requiring not only proprietary code,
but deprecated subsystems and fiddly workarounds to get it functional.

I'm reminded of [Mako's concept of
"antifeatures"](http://wiki.mako.cc/Antifeatures) -- how much
engineering time and effort went into making this system actually be
*less* stable and reliable than it would have otherwise been? How could
that work have been better-directed?

**Tags**:
[antifeatures](https://debian-administration.org/tag/antifeatures),
[flash](https://debian-administration.org/tag/flash),
[hal](https://debian-administration.org/tag/hal),
[proprietary](https://debian-administration.org/tag/proprietary),
[streaming](https://debian-administration.org/tag/streaming)

</p>

