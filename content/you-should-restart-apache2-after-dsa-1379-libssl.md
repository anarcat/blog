Title: You should restart apache2 after DSA-1379 (libssl)
Date: 2007-10-03 16:14
Author: Daniel Kahn Gillmor (dkg)
Slug: you-should-restart-apache2-after-dsa-1379-libssl

The recently announced [patch for
openssl](http://www.debian.org/security/2007/dsa-1379 "DSA-1379") does a
good thing: it looks for services known to use `libssl` and offers to
restart them for you.

However, it doesn't seem to notice `apache2`, which relies heavily on
`libssl` when `mod_ssl` is enabled. You can check to see what services
still use the old libraries (as discussed [earlier in my weblog
here](https://debian-administration.org/users/dkg/weblog/8)). If you see
`apache2` among that list, you should almost certainly do:

    /etc/init.d/apache2 restart

Alternately, if you already know that you're using `mod_ssl`, you'll
have an opportunity to add `apache2` to the list of services to be
restarted during the upgrade of `libssl`.

Many thanks to the debian security team for publishing this fix and
making it so straightforward to restart most of the affected services.
Your work is much appreciated!

**Tags**: [security](https://debian-administration.org/tag/security),
[tip](https://debian-administration.org/tag/tip), [tip security
upgrade](https://debian-administration.org/tag/tip%20security%20upgrade),
[upgrade](https://debian-administration.org/tag/upgrade)

</p>

