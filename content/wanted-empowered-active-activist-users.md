Title: Wanted: Empowered, Active, Activist Users
Date: 2009-08-27 16:34
Author: Daniel Kahn Gillmor (dkg)
Slug: wanted-empowered-active-activist-users

The [other, better-known and wiser Dan Gillmor](http://dangillmor.com/)
(disclosure: we don't just share names, we're related) has started a new
project called [Mediactive](http://mediactive.com/). His older project,
[We the Media](http://wethemedia.oreilly.com/) was about the power and
coming growth of grassroots journalism. The new project focuses on media
"consumers", instead of media "producers". Economic metaphors usually
leave me cold, but it resonates with me when [he
says](http://mediactive.com/2009/08/24/moving-along-mediactive/):

> So I’m declaring victory, albeit early, on the supply side of the
> equation. \[...\]
>
> But that doesn’t solve what may be a bigger issue: crappy demand.
>
> <p>
> We have raised several generations of passive consumers of news and
> information. That’s not good enough anymore.

The directional shift he's taking is an important one, and not just one
for an independent, grassroots media. It seems parallel in many ways to
the situation i see around free software.

The free software community has clearly demonstrated that we can build
quality, liberated tools for the public in and from the commons (though
there are admittedly tons of crap tools in the commons as well). But the
society-altering political goals of free software (that everyone should
have the right and the ability to freely use good tools fully under
their own control, a corollary of freedom 0 from [the FSF's four
freedoms](http://www.gnu.org/philosophy/free-sw.html)) are still going
to fail if the majority of the tool users shrug their shoulders and
either:

-   aren't active participants in the communities around their tools, or
-   concede to be pushed into proprietary tool use by people who are
    more interested in being proprietors than in enabling the freedom of
    their users.

Software *needs* an active and engaged userbase if it is going to become
Good Software. Your software works for you? Fine. But if you can make it
work for a dozen people who are engaged enough to say “Wouldn't it be
great if...?” or “How come it acts like...?”, you can build a tool
capable of solving problems you couldn't have imagined on your own. And
those users can help and engage each other (and still more users) too;
the community makes the tool more powerful. More powerful free tools
provide more freedom to their community of users.

Dan's new project seems to acknowledge that not everyone is going to
become a grassroots journalist themselves. And great as the dream of
free software is, we *cannot* realistically insist that everyone become
a software developer either. For either grassroots journalism or free
software to live up to their promise, we need active and engaged (or
even activist) users.

This is a tough project. i'm an active and engaged software user -- i
have to be if i want to be a decent software developer, protocol
designer, system administrator, or participant in
[debian](http://debian.org/). But it takes work and mental energy just
to be a user like that, let alone the other responsibilities. But
frankly (sorry, Dan!), i'm a terribly passive media consumer. I know
Dan's right, that i can contribute to the cause of grassroots and
distributed media (a cause i believe in, without seeing myself as a
grassroots journalist) by being a more engaged and activist user of the
media. But i don't do it currently. Why? While i'm not sure exactly
*how* to be an active or engaged user of the media, i have no doubt that
if i threw myself at the task, i could figure it out. But this kind of
learning takes time and is real work, and even if i had it all figured
out, i'm sure that being active and engaged would take more time and
work than being passive. And i already feel swamped by other obligations
and plans.

Most software users must feel the same way about their tools. How can we
reduce those barriers? How can we not only help people see that their
contributions *as users* are vitally important, but help make those
contributions and that participation easy? As far as i know, Dan (who
understands the goals, tech issues, and social concerns around software
freedom as well as any non-programmer i know) doesn't run a free
operating system himself. And i don't participate in any significant way
as an engaged user of distributed, grassroots media. That sucks.

So:

-   How do you make room for one more goal you believe in?
-   How do you make it so your own goals are appealing and easy enough
    that natural allies can participate without feeling overwhelmed?
-   What do we need to do as participants in a free software culture to
    encourage and engage active (and even activist) users?
-   What projects are there out there (by analogy with
    [Mediactive](http://mediactive.com/)) to encourage users of free
    software to be active or activist? How are they doing?

**Tags**: [free
software](https://debian-administration.org/tag/free%20software),
[politics](https://debian-administration.org/tag/politics),
[users](https://debian-administration.org/tag/users)

</p>

