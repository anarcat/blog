Title: Can you mirror-read?
Date: 2008-06-03 17:39
Author: Daniel Kahn Gillmor (dkg)
Slug: can-you-mirror-read

A high school student i work with is trying to gather data on the
prevalence of mirror-reading (the ability to easily read text that is
reversed left-to-right).

He's posted a simple webapp to gather data points for his experiment. It
should take only a couple of minutes to [try it out and give him some
data](http://leo.urbanacademy.org/). If he gets enough data to write
something up, i'll try to encourage him to publish his results as well.
