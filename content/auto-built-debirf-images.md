Title: auto-built debirf images
Date: 2011-03-29 08:00
Author: Daniel Kahn Gillmor (dkg)
Slug: auto-built-debirf-images

`jrollins` and i recently did a bunch of cleanup work on
[debirf](http://cmrg.fifthhorseman.net/wiki/debirf), with the result
that debirf 0.30 can now build all the shipped example profiles without
error (well, as long as `debootstrap --variant=fakechroot` is working
properly -- apprently that's not the case for fakechroot 2.9 in squeeze
right now, which is why i've uploaded a backport of 2.14).

To try to avoid getting into a broken state again, we set up an
autobuilder to create the images from the three example profiles
(minimal, rescue, and xkiosk) for amd64 systems. The logs for these
builds are published (with changes) nightly at:

    git://debirf.cmrg.net/debirf-autobuilder-logs

But even better, we are also publishing [the auto-generated debirf
images themselves](http://debirf.cmrg.net/autobuilds/). So, for example,
if you've got an amd64-capable machine with a decent amount of RAM
(512MiB is easily enough), you can download a rescue kernel and image
into `/boot/debirf/`, add a stanza to your bootloader, and be able to
reboot to it cleanly, without having to sort out the debirf image
creation process yourself.

We're also providing ISOs so people who still use optical media don't
have to format their own.

Please be sure to verify the checksums of the files you download. The
checksums themselves are signed by the OpenPGP key for
`Debirf Autobuilder <debirf@cmrg.net>`, which i've certified and
published to the keyserver network.

What's next? It would be nice to have auto-built images for i386 and
other architectures. And if someone has a good idea for a new example
profile that we should also be auto-building, please submit a bug to the
BTS so we can try to sort it out.

**Tags**: [debirf](https://debian-administration.org/tag/debirf)

</p>

