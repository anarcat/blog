Title: KVM, Windows XP, and Stop Error Code 0x0000007B
Date: 2012-05-29 23:49
Author: Daniel Kahn Gillmor (dkg)
Slug: kvm-windows-xp-and-stop-error-code-0x0000007b

i dislike having to run Windows as much as the next free software
developer, but like many sysadmins, i am occasionally asked to maintain
some legacy systems.

A nice way to keep these systems available (while not having to
physically maintain them) is to put them in a virtual sandbox using a
tool like kvm. While kvm makes it relatively straightforward to install
WinXP from a CD (as long as you have the proper licensing key), it is
more challenging to transition a pre-existing hardware windows XP
installation into a virtual instance, due to Windows only wanting to
boot to ide chipsets that it remembers being installed to.

In particular, booting a disk image pulled from a soon-to-be-discarded
physical disk can produce a Blue Screen of Death (BSOD) with the
message:

    Stop error code 0x0000007B

or

    (INACCESSABLE_BOOT_DEVICE)

This seems like it's roughly the equivalent (in a standard debian
GNU/Linux environment) of specifying `MODULES=dep` in
`/etc/initramfs-tools/initramfs.conf`, and then trying to swap out all
the hardware.

At first blush, Microsoft's knowledge base [suggests doing an in-place
upgrade or full repartition and
reinstall](http://support.microsoft.com/kb/316401), which are both
fairly drastic measures -- you might as well just start from scratch,
which is exactly what you *don't* want to have to do for a nursed-along
legacy system which no one who originally set it up is even with the
organization any more.

Fortunately, a bit more digging in the Knowledge Base turned up [an
unsupported set of steps](http://support.microsoft.com/kb/314082) that
appears to be the equivalent of setting `MODULES=most` (at least for the
IDE chipsets). Running this on the old hardware before imaging the disk
worked for me, though i did need to re-validate Windows XP after the
reboot by typing in the long magic code again. i guess they're keying it
to the hardware, which clearly changed in this instance.

Such silliness to spend time working around, really, when i'd rather be
spending my time working on free software. :/

**Tags**: [bugs](https://debian-administration.org/tag/bugs),
[windows](https://debian-administration.org/tag/windows)

</p>

