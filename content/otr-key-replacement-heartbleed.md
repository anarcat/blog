Title: OTR key replacement (heartbleed)
Date: 2014-04-14 18:43
Author: Daniel Kahn Gillmor (dkg)
Slug: otr-key-replacement-heartbleed

I'm replacing my OTR key for XMPP because of heartbleed (see below).

If the plain ASCII text below is mangled beyond verification, you can
retrieve a copy of it [from my web
site](https://dkg.fifthhorseman.net/dkg-xmpp-otr-key-2014.txt) that
should be able to be verified.

    -----BEGIN PGP SIGNED MESSAGE-----Hash: SHA512OTR Key Replacement for XMPP dkg@jabber.org===========================================Date: 2014-04-14My main XMPP account is dkg@jabber.org.I prefer OTR [0] conversations when using XMPP for privatediscussions.I was using irssi to connect to XMPP servers, and irssi relies onOpenSSL for the TLS connections.  I was using it with versions ofOpenSSL that were vulnerable to the "Heartbleed" attack [1].  It'spossible that my OTR long-term secret key was leaked via this attack.As a result, I'm changing my OTR key for this account.The new, correct OTR fingerprint for the XMPP account at dkg@jabber.org is:  F8953C5D 48ABABA2 F48EE99C D6550A78 A91EF63DThanks for taking the time to verify your peers' fingerprints.  Securecommunication is important not only to protect yourself, but also toprotect your friends, their friends and so on.Happy Hacking,  --dkg  (Daniel Kahn Gillmor)Notes:[0] OTR: https://otr.cypherpunks.ca/[1] Heartbleed: http://heartbleed.com/-----BEGIN PGP SIGNATURE-----Version: GnuPG v1iQJ8BAEBCgBmBQJTTBF+XxSAAAAAAC4AKGlzc3Vlci1mcHJAbm90YXRpb25zLm9wZW5wZ3AuZmlmdGhob3JzZW1hbi5uZXRFQjk2OTEyODdBN0FEREUzNzU3RDkxMUVBNTI0MDFCMTFCRkRGQTVDAAoJEKUkAbEb/fpcYwkQAKLzEnTV1lrK6YrhdvRnuYnhBh9Ad2ZY44RQmN+STMEnCJ4OWbn5qx/NrziNVUZN6JddrEvYUOxME6K0mGHdY2KRyjLYudsBuSMZQ+5crZkE8rjBL8vDj8Dbn3mHyT8bAbB9cmASESeQMu96vni15ePd2sB7iBofee9YAoiewI+xRvjo2aRX8nbFSykoIusgnYG2qwo2qPaBVOjmoBPB5YRIPkN0/hAh11Ky0qQ/GUROytp/BMJXZx2rea2xHs0mplZLqJrX400u1Bawllgz3gfVqQKKNc3st6iHf3F6p6Z0db9NRq+AJ24fTJNcQ+t07vMZHCWM+hTelofvDyBhqG/rl8e4gdSh/zWTR/7TR3ZYLCiZzU0uYNd0rE3CcxDbnGTUS1ZxooykWBNIPJMl1DUEzzcrQleLS5tna1b9la3rJWtFIATyO4dvUXXa9wU3c3+Wr60cSXbsK5OCct2KmiWYfJme0bpM5m1j7B8QwLzKqy/+YgOOJ05QDVbBZwJn1B7rvUYmb968yLQUqO5Q87L4GvPB1yY+2bLLF2oFMJJzFmhKuAflslRXyKcAhTmtKZY+hUpxoWuVa1qLU3bQCUSEMlC4Hv6vaq14BEYLeopoSb7THsIcUdRjho+WEKPkryj6aVZM5WnIGIS/4QtYvWpk3UsXFdVZGfE9rfCOLf0F=BGa1-----END PGP SIGNATURE-----

</p>

