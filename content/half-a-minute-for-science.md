Title: half a minute for science!
Date: 2013-09-25 21:41
Author: Daniel Kahn Gillmor (dkg)
Slug: half-a-minute-for-science

A friend is teaching a class on data analysis. She is building a simple
and rough data set for the class to examine, and to spur discussion. You
can contribute in half a minute! Here's how:

1.  get a stopwatch or other sort of timer (whatever device you're
    reading this on probably has such a thing).
2.  start the timer, but don't look at it.
3.  wait for what you think is 30 seconds, and then look at the timer
4.  how many actual seconds elapsed?

The data doesn't need to be particularly high-precision (whole second
values are fine). The other data points my friend is looking for are age
(in years, again, whole numbers are fine) and gender.

You can send me your results by e-mail, (i suspect you can find my
address if you're reading this blog). Please put "half a minute for
science" in the subject line, and make sure you include:

-   actual seconds elapsed
-   age in years
-   gender

Science thanks you!

**Tags**: [science](https://debian-administration.org/tag/science)

</p>

