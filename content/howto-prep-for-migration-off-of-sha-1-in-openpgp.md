Title: HOWTO prep for migration off of SHA-1 in OpenPGP
Date: 2009-05-06 06:15
Author: Daniel Kahn Gillmor (dkg)
Slug: howto-prep-for-migration-off-of-sha-1-in-openpgp

Last week at eurocrypt, a small group of researchers announced a fairly
serious [attack against the SHA-1 digest
algorithm](http://eurocrypt2009rump.cr.yp.to/837a0a8086fa6ca714249409ddfae43d.pdf),
which is used in many cryptosystems, including OpenPGP. The general
consensus is that we should be “moving in an orderly fashion toward the
theater exits,” deprecating SHA-1 where possible with an eye toward
abandoning it soon (one point of reference: [US gov't federal agencies
have been directed to cease all reliance on SHA-1 by the end of
2010](http://csrc.nist.gov/groups/ST/hash/statement.html), and this
directive was issued *before* the latest results).

Since Debian relies heavily on OpenPGP and other cryptographic
infrastructure, i'll be blogging about how Debian users can responsibly
and carefully migrate toward better digests. This post focuses on some
first steps for users of `gpg`, and for Debian Developers and Debian
Maintainers in particular.

The good news is that `gpg` and `gpg2` both support digest algorithms
from the stronger SHA-2 family: SHA512, SHA384, SHA256, and SHA224.

By using these stronger digest algorithms some of your signatures may be
un-readable by users of older software. However, `gpg` and PGP (a
proprietary implementation) have both had support for at least SHA256
for well over 5 years. Debian's
[gnupg](http://packages.debian.org/gnupg) packages have supported the
full SHA-2 family since sarge.

However, most existing signatures in today's Web of Trust were made over
the SHA-1 digest algorithm, which means that abandoning it immediately
would cause the Web of Trust as we know it to evaporate. So we need to
rely on SHA-1-based signatures until a reasonably-fleshed-out Web of
Trust based on stronger digests is in place. Since we don't want to have
to rely on SHA-1 for too much longer, we need to collectively start the
transition now.

So what can you do to help facilitate the move away from SHA-1? I'll
outline three steps that current `gpg` users can do today, and then i'll
walk through how to do each one:

-   start making data signatures and web-of-trust certifications using
    stronger digests,
-   explicitly state your preferences for stronger digests when
    receiving private communications, and
-   If you are currently using a 1024-bit DSA primary key (which relies
    for signatures on a 160-bit hash, traditionally SHA-1), transition
    to a new 2048-bit RSA key.

The first two are simple, quick, and painless actions. You'll be done
with them in minutes! The third is tougher, and while you can start it
today, key transitions take a little bit of time to complete. Read on
for a HOWTO!

This entry has been truncated [read the full
entry](https://debian-administration.org/users/dkg/weblog/48).

**Tags**:
[cryptography](https://debian-administration.org/tag/cryptography),
[gpg](https://debian-administration.org/tag/gpg),
[openpgp](https://debian-administration.org/tag/openpgp),
[security](https://debian-administration.org/tag/security),
[sha1](https://debian-administration.org/tag/sha1),
[tip](https://debian-administration.org/tag/tip)

</p>

