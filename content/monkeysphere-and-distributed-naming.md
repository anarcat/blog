Title: monkeysphere and distributed naming
Date: 2010-10-06 23:19
Author: Daniel Kahn Gillmor (dkg)
Slug: monkeysphere-and-distributed-naming

Roland Mas writes [an interesting article about decentralized
naming](http://roland.entierement.nu/blog/2010/10/02/for-a-truly-acentric-internet.html),
in which he says:

> [Monkeysphere](http://web.monkeysphere.info/) aims at adding a web of
> trust to the SSL certificates system, but the CA chain problem seems
> to persist (although I must admit I'm not up to speed with the actual
> details).

Since i'm one of the Monkeysphere developers, i figure i should respond!

Let me clarify that Monkeysphere doesn't just work in places where X.509
(the SSL certificate system) works. It works in other places too (like
SSH connections). And I don't think that the CA chain problem that
remains in Monkeysphere is anything like the dangerous [mess that common
X.509 usage has given
us](http://lair.fifthhorseman.net/~dkg/tls-centralization). I do think
that at some level, people need to think about who is introducing them
to other people -- visual or human-comprehensible representations of
public key material are notoriously difficult to make unspoofable.

On the subject of distributed naming: OpenPGP already allows distributed
naming: every participant in the WoT is allowed to assert that any given
key maps to any given identity. Duplicates and disagreements can exist
just fine. How an entity decides to certify another entity's ID without
a consensus global namespace is a tough one, though. If i've always been
known as "John Smith" to my friends, and someone else has also been
known as "John Smith" to his friends, our friends aren't actually
disagreeing or in conflict -- it's just that neither of us has a unique
name. The trouble comes when someone new wants to find "John Smith" --
which of us should they treat as the "correct" one?

I think the right answer probably has to do with who they're actually
looking for, which has to do with *why* they're looking for someone
named "John Smith". If they're looking for John Smith because the word
on the street is that John Smith is a good knitter and they need a pair
of socks, they can just examine what information we each publish about
ourselves, and decide on a sock-by-sock basis which of us best suits
their needs.

But if they're looking for "John Smith" because their cousin said "hey,
i know this guy John Smith. I think you would like to argue politics
over a beer with him", then what matters is the *introduction*. And
OpenPGP handles that just fine -- if their cousin has only ever met a
single John Smith, that's the right one. If their cousin has met several
John Smiths, then the searcher would do well to ask their cousin some
variant of "hey, do you mean John Smith or John Smith ", or even "do you
mean the John Smith who Molly has met, or the one who Charles has met?"
(assuming that Molly and Charles have each only certified one John Smith
in common with the cousin, and not the same one as each other), or to
get a real-time introduction to a particular John Smith, where his
specific key is somehow recordable by the searcher for future
conversations (or beer drinking). This is what we do in the real world
anyway. We currently lack good UIs for doing this over the network, but
the certification infrastructure is in place already.

What we're lacking in infrastructure, though, is a way to have a
distributed *addressing*. Roland's proposal was to publish addresses
corresponding to cryptographic identities within some DNS zone, or in
freenet or gnutella. Another approach (piggybacking on existing
infrastructure) would be to include IP address information in the
OpenPGP self-certification, so the holder of the name could claim
exactly their own IP address. This could be distributed through the
keyserver network, just like other updates are today, and it could be
done simply and immediately with a well-defined [OpenPGP
notation](http://tools.ietf.org/html/rfc4880#section-5.2.3.16). I'd be
happy to talk to interested people about how to specify such a notation,
and what possible corner cases we might run into. Drop a note here, or
mail [the Monkeysphere mailing
list](http://web.monkeysphere.info/community/) or hop onto
[`#monkeysphere` on irc.oftc.net](irc://irc.oftc.net#monkeysphere)

**Tags**:
[authentication](https://debian-administration.org/tag/authentication),
[distributed
naming](https://debian-administration.org/tag/distributed%20naming),
[identity](https://debian-administration.org/tag/identity),
[monkeysphere](https://debian-administration.org/tag/monkeysphere)

</p>

