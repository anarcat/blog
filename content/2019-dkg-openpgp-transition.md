Title: New OpenPGP certificate for dkg, 2019
Date: 2019-01-19 02:49:53-0500
Author: Daniel Kahn Gillmor (dkg)
Slug: 2019-dkg-openpgp-transition

Update
------

I've scrapped my first try at a new OpenPGP certificate for 2019 (the
one I published yesterday).  See the history discussion at the bottom
of this post for details.  This blogpost has been updated to reflect
my revised attempt.

2019 OpenPGP transition (try 2)
------------====---------------

My old OpenPGP certificate will be 12 years old later this year.  I'm
transitioning to a new OpenPGP certificate.

You might know my old OpenPGP certificate as:

```
pub   rsa4096 2007-06-02 [SC] [expires: 2019-06-29]
      0EE5BE979282D80B9F7540F1CCD2ED94D21739E9
uid          Daniel Kahn Gillmor <dkg@fifthhorseman.net>
uid          Daniel Kahn Gillmor <dkg@debian.org>
```

My new OpenPGP certificate is:

```
pub   ed25519 2019-01-19 [C] [expires: 2021-01-18]
      C4BC2DDB38CCE96485EBE9C2F20691179038E5C6
uid          Daniel Kahn Gillmor <dkg@fifthhorseman.net>
uid          Daniel Kahn Gillmor <dkg@debian.org>

```

If you've certified my old certificate, I'd appreciate your certifying
my new one.  Please do confirm by contacting me via whatever channels
you think are most appropriate (including in-person if you want to
share food or drink with me!) before you re-certify, of course.

I've published the new certificate to the SKS keyserver network, as
well as to [my personal
website](https://dkg.fifthhorseman.net/dkg-openpgp.key) -- you can
fetch it like this:

    wget -O- https://dkg.fifthhorseman.net/dkg-openpgp.key | gpg --import

A copy of this transition statement signed by both the old and new
certificates is available [on my
website](https://dkg.fifthhorseman.net/2019-dkg-openpgp-transition.txt),
and you can also find [further explanation about technical details,
choices, and rationale on my
blog](https://dkg.fifthhorseman.net/blog/2019-dkg-openpgp-transition.html).


Technical details
-----------------

I've made a few decisions differently about this certificate:

### Ed25519 and Curve25519 for Public Key Material

I've moved from 4096-bit RSA public keys to the [Bernstein elliptic
curve 25519](https://cr.yp.to/ecdh.html) for all my public key
material ([EdDSA](https://en.wikipedia.org/wiki/EdDSA) for signing,
certification, and authentication, and
[Curve25519](https://en.wikipedia.org/wiki/Curve25519) for
encryption).  25519 appears to be significantly stronger than any
cryptanalytic attack known to the public.  (4096-bit RSA may be
marginally stronger cryptographically than curve 25519, but both of
them are well beyond the threshhold of what we know how to break with
today's machinery)

Additionally, elliptic curve keys and the signatures associated with
them are tiny compared to 4096-bit RSA.  I certified my new cert with
my old one, and well over half of the new certificate is just
certifications from the old key because they are so large.

This size advantage makes it easier for me ship the public key
material (and signatures from it) in places that would be more awkward
otherwise.  See the discussion about Autocrypt below.

### Split out ACLU identity

Note that my old certificate included some additional identities,
including job-specific e-mail addresses.  I've split out my
job-specific cryptographic credentials to a different OpenPGP
certificate entirely.  If you want to mail me at <dkg@aclu.org>, you
can use the certificate with fingerprint
`888E6BEAC41959269EAA177F138F5AB68615C560` (which is also published on
[my work bio page](https://www.aclu.org/bio/daniel-kahn-gillmor)).

This is in part because the folks who communicate with me at my ACLU
address are more likely to have old or poorly-maintained e-mail
systems than other people I communicate with, and they might not be
able to handle curve 25519.  So the ACLU keys use 3072-bit RSA, which
is universally supported by any plausible OpenPGP implementation.

This way I can experiment with being more forward-looking in my free
software and engineering community work, and shake out any bugs that I
might find there, before cutting over the e-mails that come in from
more legal- and policy-focused colleagues.

### Isolated Subkey Capabilities

In my new certificate, the primary key is designated
certification-only.  There are three subkeys, one each for
authentication, encryption, and signing.  The primary key also has a
longer expiration time (2 years as of this writing), while the subkeys
have 1 year expiration dates.

Isolating this functionality helps a little bit with security (I can
take the certification key entirely offline while still being able to
sign non-identity data), and it also offers a pathway toward having a
more robust subkey rotation schedule.  As I build out my tooling for
subkey rotation, i'll probably make a few more blog posts about that.

### Autocrypt-friendly

Finally, several of these changes are related to [the Autocrypt
project](https://www.autocrypt.org/), a really great collaboration of
a group of mail user agent developers, designers, UX experts,
trainers, and users, who are providing guidance to make encrypted
e-mail something that normal humans can use without having to think
too much about it.

Autocrypt treats the OpenPGP certificate User IDs as merely
decorative, but its [recommended form of the User
ID](https://autocrypt.org/level1.html#openpgp-based-key-data) for an
OpenPGP certificate is just the e-mail address wrapped in angle
brackets.  Unfortunately, I didn't manage to get that particular form
of User ID into this certificate at this time (see discussion of split
User IDs below).

Autocrypt is also [moving toward 25519 elliptic curve
keys](https://github.com/autocrypt/autocrypt/pull/393), so this gives
me a chance to exercise that choice.

I'm proud to be associated with the Autocrypt project, and have been
helping to shepherd some of the Autocrypt functionality into different
clients (my work on my own MUA of choice,
[notmuch](https://notmuchmail.org) is currently stalled, but I hope to
pick it back up again soon).  Having an OpenPGP certificate that works
well with Autocrypt, and that I can stuff into messages even from
clients that aren't fully-Autocrypt compliant yet is useful to me for
getting things tested.

### Documenting workflow vs. tooling

Some people may want to know "how did you make your OpenPGP cert like
this?" For those folks, i'm sorry but this is not a step-by-step
technical howto.  I've read far too many "One True Way To Set Up Your
OpenPGP Certificate" blog posts that haven't aged well, and i'm not
confident enough to tell people to run the weird arbitrary commands
that I ran to get things working this way.

Furthermore, I don't *want* people to have to run those commands.

If I think there are sensible ways to set up OpenPGP certificates, I
want those patterns built into standard tooling for normal people to
use, without a lot of command-line hackery.

So if i'm going to publish a "how to", it would be in the form of
software that I think can be sensibly maintained and provides a sane
user interface for normal humans.  I haven't written that tooling yet,
but I need to change certs first, so for now you just get this blog
post in English.  But feel free to tell me what you think I could do
better!

History
-------

This is my second attempt at an OpenPGP certificate transition in
2019.  My earlier attempt uncovered a bunch of tooling issues with
split-out User IDs.  The original rationale for trying the split, and
the problems I found are detailed below.

### What were Separated User IDs?

My earlier attempt at a new OpenPGP certificate for 2019 tried to do
an unusual thing with the certificate User IDs. Rather than two User
IDs:

 * `Daniel Kahn Gillmor <dkg@fifthhorseman.net>`
 * `Daniel Kahn Gillmor <dkg@debian.org>`

the (now revoked) earlier certificate had the name separate from the
e-mail addresses, making three User IDs:

 * `Daniel Kahn Gillmor`
 * `dkg@fifthhorseman.net`
 * `dkg@debian.org`

There are a couple reasons I tried this.

One reason is to simplify the certification process.  Traditional
OpenPGP User ID certification is an all-or-nothing process: the
certifier is asserting that both the name and e-mail address belong to
the identified party.  But this can be tough to reason about.  Maybe
you know my name, but not my e-mail address.  Or maybe you know my
over e-mail, but aren't really sure what my "real" name is (i'll leave
questions about what counts as a real name to a more philosophical
blog post).  You ought to be able to certify them independently.  Now
you can, since it's possible to certify one User ID independently of
another.

Another reason is because I planned to use this certificate for
e-mail, among other purposes.  In e-mail systems, the human name is a
confusing distraction, as the real underlying correspondent is the
e-mail address.  E-mail programs should definitely allow their users
to couple a memorable name with an e-mail address, but it should be
more like a [petname](https://en.wikipedia.org/wiki/Petname).  The
bundling of a human "real" name with the e-mail address by the User ID
itself just provides more points of confusion for the mail client.

If the user communicates with a certain person by e-mail address, the
certificate should be bound to the e-mail protocol address on its own.
Then the user themselves can decide what other monikers they want to
use for the person; the User ID shouldn't force them to look at a
"real" name just because it's been bundled together.


Alas, putting this attempt into public practice uncovered several gaps
in the OpenPGP ecosystem.

User IDs without an e-mail address are often ignored, mishandled, or
induce crashes:

 * [emacs](https://www.gnu.org/software/emacs/) [chokes when
   contemplating a User ID without an e-mail
   address](https://debbugs.gnu.org/cgi/bugreport.cgi?bug=34121)

 * [pius](https://www.phildev.net/pius/) [fails to certify User IDs
   without an e-mail
   address](https://github.com/jaymzh/pius/issues/109)

 * [monkeysign](https://0xacab.org/monkeysphere/monkeysign) [certifies
   User IDs without an e-mail address, but sends them into the
   void](https://0xacab.org/monkeysphere/monkeysign/issues/65).

 * [gnome-keysign-sign-key](https://wiki.gnome.org/Apps/Keysign)
   [discards certifications over User IDs that have no e-mail
   address](https://github.com/gnome-keysign/gnome-keysign/issues/71)

 * [schleuder](https://schleuder.org/) [dis-associates a certificate
   from all e-mail addresses if the primary User ID lacks an e-mail
   adddress](https://0xacab.org/schleuder/schleuder/issues/395)

And User IDs that are a raw e-mail address (without enclosing
angle-brackets) tickle additional problems.

 * [pius](https://www.phildev.net/pius/) [fails to certify User IDs
   that are just a raw e-mail
   address](https://github.com/jaymzh/pius/issues/111)
 
 * [schleuder](https://schleuder.org/) [fails to process User IDs
   which are bare e-mail
   addresses](https://0xacab.org/schleuder/schleuder/issues/395)

Finally, [Monkeysphere](https://web.monkeysphere.info)'s ssh user
authentication mechanism typically works on a single User ID at a
time.  There's no way in Monkeysphere to say "authorize access to
account foo by any OpenPGP certificate that has a valid User ID `Alice
Jones` *and* a valid User ID `<alice@example.org>`.  I'd like to keep
the `~/.monkeysphere/authorized_user_ids` that I already have in place
working OK.  I have enough technical debt to deal with for
Monkeysphere (including that it only handles RSA currently) that I
don't need the additional headache of reasoning about split/joint User
IDs too.

Because of all of these issues, in particular the schleuder bugs, i'm
not ready to use a split User ID OpenPGP certificate on today's
Internet, alas. I have revoked the OpenPGP certificate that had split
User IDs and started over with a new certificate with a more standard
User ID layout, as described above.  Better to rip off the band-aid
quickly!
