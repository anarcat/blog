Title: USAA Deposit@Home: bad engineering and terrible UX
Date: 2011-04-28 06:59
Author: Daniel Kahn Gillmor (dkg)
Slug: usaa-deposithome-bad-engineering-and-terrible-ux

I use [USAA](https://www.usaa.com) for some of my finances. They
specialize in remote banking (i've never been in a physical branch).
Sadly, they can still be pretty clueless about how to use the web
properly. My latest frustration with them was trying to use their
[Deposit@Home](https://www.usaa.com/inet/pages/bank_deposit) service,
where you scan your checks and upload them.

No problem, right? I've got a scanner and a web browser and i know how
to use them both. Ha ha. Upon first connecting, i'm rejected, and i find
the absurd [System
Requirements](https://www.usaa.com/inet/pages/help_deposit_system_requirements)
-- only Windows and Mac, and only certain versions of certain browsers.
You also need Sun's Java plugin, apparently.

Deliberately naïve, i call their helpdesk and ask them if they could
just give me a link to let me upload my scanned checks. They tell me
that they want 200dpi images, and then give an absurd runaround that
includes references to [the Gramm-Leach-Bliley
Act](https://secure.wikimedia.org/wikipedia/en/wiki/Gramm-Leach-Bliley_Act)
as the reason they need to limit the system to Windows or Mac, and that
security is the reason they need to control the scanner directly
(apparently your browser can control your local scanner on Windows?
yikes). But they let slip that Mac users don't have the scanner
controlled directly, and can just upload images (apparently the federal
law doesn't cover them or something). Preposterous silliness.

### The Workaround

Of course, it turns out you can get it working on Debian GNU/Linux,
mainly by telling them what they want to hear ("yes sir, i'm running Mac
OS"), but you'll also have to run Sun's Java (non-free software) to do
it, since their Java uploader fails with [nasty errors when using
icedtea6-plugin](http://lair.fifthhorseman.net/~dkg/personal/usaa.java.err.txt).

I set up a dedicated system user for this goofiness, since i'm going to
be running apparently untrustworthy applets on top of non-free software.
I run a separate instance of iceweasel as that user; all configuration
described is for that user's homedir. If you do this yourself, You'll
need to decide if you want the same level of isolation for yourself.

So i have the choice of installing
[sun-java6-plugin](http://packages.debian.org/sun-java6-plugin) from
`non-free` and having the plugin installed for all web browsers, or just
doing a per-user install of java for my dedicated user (and avoiding the
plugin for my normal user). I opted for the latter. As the dedicated
user, I fetched the self-extracting variant from
[java.com](https://www.java.com/en/download/manual.jsp), unpacked it,
and added it to the iceweasel plugins:

    chmod a+x ~/Download/jre-6u25-linux-i586.binmkdir -p ~/lib ~/.mozilla/pluginscd ~/lib~/Download/jre-6u25-linux-i586.binln -s ~/lib/jre*/lib/*/libnpjp2.so ~/.mozilla/plugins/

Then i closed iceweasel and restarted it. In the relaunched iceweasel
sesson, I told Iceweasel 4 to pretend that it was actually Firefox 3.6
on a Mac. I did this by going to `about:config` (checking the box that
says i know what i'm doing), right-clicking, and choosing "new &gt;&gt;
string". The new variable name is
[`general.useragent.override`](http://kb.mozillazine.org/Firefox_:_FAQs_:_About:config_Entries#General.)
and i found that setting it to the following (insane, false) value
worked for me:

    Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; en-US; rv:1.9.2.3) Gecko/20060426 Firefox/3.5.18

Note that this configuration might make [hotmail think that you are a
mobile device](https://debian-administration.org/users/dkg/weblog/63).
:P If you try to use this browser profile for anything other than
visiting USAA, you might want to remove this setting, or install
[`xul-ext-useragentswitcher`](http://packages.debian.org/xul-ext-useragentswitcher)
to be able to set it more easily than using `about:config`.

Once these changes were made, I was able to log into USAA, use the
Deposit@Home service to upload my 200dpi grayscale scans. I guess they
think i'm a Mac user now.

### The Followup

After completing the upload, I wrote them this review (i doubt they'll
post it):

> The Deposit@Home service has great potential. Unfortunately, it
> currently appears to be overengineered and unnecessarily restrictive.
>
> The service requires two scans (front and back) of the deposited
> check, at 200dpi, in grayscale or black-and-white, in jpeg format,
> reasonably-cropped. The simplest way to do this would be to show some
> examples of good scans and bad scans, and provide two file upload
> forms.
>
> Once the user uploaded their images, the web site could run its own
> verification, and display them back for the user to confirm,
> optionally using a simple javascript-based image-cropper if any image
> seems wrong-sized. This would work fine with any reasonable browser on
> any OS.
>
> Instead, Deposit@Home requires the user to present a User-Agent header
> claiming to be from specific versions of Mac or Windows, running
> certain (older) versions of certain browsers, and requires the use of
> Sun's Java plugin.
>
> <p>
> Entirely unnecessary system requirements to do a simple task.
> Disappointing. :(

### Acknowledgements

I found [good background for this approach on the ubuntu
forums](http://ubuntuforums.org/showthread.php?t=1454931).

### The Takeaway

I continue to be frustrated and annoyed by organizations that haven't
yet embraced the benefits of the open web. Clearly, USAA has spent a lot
of money engineering what they think is a certain experience for their
users. However, they didn't design with standard web browsers in mind,
so they appear to have boxed themselves into a corner where they think
they have to test and approve of the entire software stack running on
the end-user's machine.

This is not only foolish -- it's impossible. When you're designing a
web-based application, just design it for the web. Keep it simple. If
you want to offer some snazzy java-hooked-into-your-scanner insanity, i
will only have a mild objection: it seems like a waste of time and
engineering effort. My objection is much stronger if your
snazzy/incompatible absurdity *is the only way to use your service*. A
simple, web-based, browser-agnostic interface should be available to all
your clients.

Even more aggravating is the claim that they don't think they should
engineer for everyone. I was told during the runaround that they would
only support Linux if 4% of their users were using Linux (which they
don't think is the case at the moment -- if you are a USAA customer, and
you use something other than Mac and Windows, please tell them so). I
tried to tell them that I wasn't asking for Linux support; i was asking
for *everyone* support. If you just use generic engineering in the first
place, there's no extra expense for special-casing other users. But they
couldn't understand.

And now, since i'll need to lie to them in my User Agent string every
time i want to deposit a check online, those visits won't even show up
in their logs to be counted. "Our web site deliberately disables itself
for \$foo users; we haven't written it for them; but that's OK, we don't
have any \$foo users anyway" is a nasty self-fulfilling prophecy. Why
would you do that?
