Title: Interpreting vmstat's wait stats on linux 2.6
Date: 2007-02-09 17:19
Author: Daniel Kahn Gillmor (dkg)
Slug: interpreting-vmstats-wait-stats-on-linux-26

I'm trying to figure out if the linux 2.6 kernel's “wait” cycles include
cycles waiting on network I/O or specifically just waiting on disk
access.

If network I/O is included, it would change my interpretation of a
system with spiking “wait” percentages. Maybe the disk controllers
aren't saturated; instead, it could be that processes are connecting to
remote hosts which not responding, or delaying their responses.

Does anyone know? What would be the best way to go about finding the
answer to this? I can imagine a handful of different meanings: Time CPU
is idle while at least one process is:

-   waiting on *any* I/O, regardless of subsystem (including
    stdin/stdout for interactive processes?)
-   waiting on I/O that is eventually served by the disks (i.e. no
    network)
-   waiting on I/O from the filesystem (this might be different from the
    above: more in the case of NFS, tmpfs, sshfs, etc, or less in the
    case of swap or other non-filesystem disk use)

I'm sure there are other possible meanings too. Some [AIX `vmstat`
notes](http://publib.boulder.ibm.com/infocenter/pseries/v5r3/topic/com.ibm.aix.prftungd/doc/prftungd/vmstat_command.htm)
seem to imply it is most like the third option above, but AIX and linux
are different enough (and this question is probably kernel-specific
enough) that i want to be sure about my particular O/S.

Background for folks who aren't clear what i'm asking about: `vmstat`
(and other tools) report CPU time broken out into `us`, `sy`, `id`, and
`wa`. The `wa` (wait) percentage is described in `man vmstat` this way:

> `wa: Time spent waiting for IO. Prior to Linux 2.5.41, included in idle.`

Unfortunately, this doesn't go into enough detail for me, hence this
post...

**Tags**: [vmstat](https://debian-administration.org/tag/vmstat)

</p>

