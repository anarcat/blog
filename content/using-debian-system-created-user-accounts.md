Title: using debian system-created user accounts?
Date: 2007-09-14 21:47
Author: Daniel Kahn Gillmor (dkg)
Slug: using-debian-system-created-user-accounts

Does anyone use the `backup` user for doing local system backups? or do
you create a new user specifically for that?

I ask because `backup` is debian-specific uid 34, provided by the
`base-passwd` package. But reading
`/usr/share/doc/base-passwd/users-and-groups.txt.gz`, i find only this:

    backup    Presumably so backup/restore responsibilities can be locally delegated to    someone without full root permissions?    HELP: Is that right? Amanda reportedly uses this, details?

So fellow admins: do you use debian-allocated accounts ([i.e., with uid
&lt;
100](http://www.debian.org/doc/manuals/system-administrator/ch-sysadmin-users.html))
(other than root) for any local (non-distro) purpose? If so, when do you
do so? if not, why not?

**Tags**: [backup](https://debian-administration.org/tag/backup),
[systemuser](https://debian-administration.org/tag/systemuser)

</p>

