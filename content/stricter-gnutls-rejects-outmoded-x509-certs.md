Title: Stricter GnuTLS rejects outmoded X.509 certs
Date: 2009-02-15 17:20
Author: Daniel Kahn Gillmor (dkg)
Slug: stricter-gnutls-rejects-outmoded-x509-certs

Recently, several people noticed that GnuTLS behavior did not match its
documentation with respect to two significant security concerns, both
regarding X.509 certificate validation:

-   [Certificates with MD5 digests were considered acceptable for
    validation](http://thread.gmane.org/gmane.comp.encryption.gpg.gnutls.devel/3332).
    They should not be, because of [known weaknesses in that digest
    algorithm](http://www.win.tue.nl/hashclash/rogue-ca/).
-   [Version 1 X.509 certificates were being accepted as Certificate
    Authorities if placed in the list of trusted
    certificates](http://thread.gmane.org/gmane.comp.encryption.gpg.gnutls.devel/3351/focus=3365).
    It is impossible to distinguish between a V1 Authority certificate
    and a V1 End Entity certificate. This makes placement of such a cert
    in the trusted certificates list dangerously ambiguous, because you
    don't want holders of an end entity certificate to be able to act as
    certificate authorities)

GnuTLS has been fixed upstream, and [a fix to at least the latter
problem has already propagated into etch via a security
upload](http://www.debian.org/security/2009/dsa-1719). The lenny
packages should already behave as documented (no MD5 digests accepted in
cert validation, v1 certificates not explicitly acceptable as
authorities).

*However*, this means that if you use GnuTLS-linked tools to connect to
systems whose certificate chains rely on either MD5 digests (in anything
but the root certificate) or on v1 certificates for any of the
certificate authorities, your connections may fail due to this stricter
validation.

There are already several bug reports about [broken LDAP
connections](http://bugs.debian.org/514807) and [broken mail
connections](http://bugs.debian.org/514578) due to these problems, and
there have been [reasonable concerns raised on
debian-release](http://lists.debian.org/debian-release/2009/02/msg00390.html)
about this.

So what can you do to make sure that the infrastructure you rely on or
maintain does not depend on these outmoded and insecure features of
X.509? Read on for concrete steps for administrators,
developers/maintainers, and end users...

This entry has been truncated [read the full
entry](https://debian-administration.org/users/dkg/weblog/42).

**Tags**: [gnutls](https://debian-administration.org/tag/gnutls),
[ldap](https://debian-administration.org/tag/ldap),
[openldap](https://debian-administration.org/tag/openldap),
[security](https://debian-administration.org/tag/security),
[tip](https://debian-administration.org/tag/tip),
[x.509](https://debian-administration.org/tag/x.509)

</p>

