Title: target disk mode (sbp-2 mass storage host mode?)
Date: 2009-01-23 19:24
Author: Daniel Kahn Gillmor (dkg)
Slug: target-disk-mode-sbp-2-mass-storage-host-mode

One very convenient feature of Apple's hardware is the ability for their
workstations to enter [target disk
mode](http://en.wikipedia.org/wiki/Target_Disk_Mode). This effectively
transforms the machine into a glorified
[ieee1394](http://en.wikipedia.org/wiki/IEEE_1394_interface) (firewire)
block device, which can then be manipulated from outside the machine
using the standard [SBP-2](http://en.wikipedia.org/wiki/SBP-2) protocol.

Is there a way to provide this same functionality from a running
GNU/Linux machine? For example, i have a GNU/Linux system with a block
device attached to it. I would prefer if the block device was available
to a neighboring machine, but (for whatever reason) i'm unable to
physically move it. However, i'm able to link the two machines via a
simple ieee1394 connection. A "virtual target disk mode" server (or
would it be better to say "an SBP-2 mass storage target service over an
ieee1394 link"?) would be really useful.

One thing that occurs to me is that i could do some sort of networking
abstraction over the link (using
[eth1394](http://www.linux1394.org/eth1394.php)?), and then use
something like [vblade](http://packages.debian.org/vblade) (an
[ATA-over-Ethernet](http://en.wikipedia.org/wiki/ATA_over_Ethernet)
target service) to provide a virtual block device to the remote host.
However, this requires the remote host to run an operating system
capable of dealing with these (more obscure) protocols, and i'd like
this to work for any remote machine that knows how to deal with generic
SBP-2 ieee1394 mass storage.

I know that in general GNU/Linux is at least as powerful and capable as
the firmware that Apple ships ;) But it's possible that our community
just hasn't gotten around to implementing something like this. Is this
the case? My attempts to search for it haven't turned up anything, but
it's entirely possible that i'm reading the wrong docs (or reading the
docs wrong). Any pointers?

**Tags**: [firewire](https://debian-administration.org/tag/firewire),
[ieee1394](https://debian-administration.org/tag/ieee1394),
[sbp2](https://debian-administration.org/tag/sbp2),
[vblade](https://debian-administration.org/tag/vblade)

</p>

