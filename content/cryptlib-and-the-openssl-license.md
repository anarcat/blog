Title: CryptLib and the OpenSSL License
Date: 2011-03-16 04:40
Author: Daniel Kahn Gillmor (dkg)
Slug: cryptlib-and-the-openssl-license

I spent part of today looking at packaging [Peter Gutmann's
CryptLib](http://www.cs.auckland.ac.nz/~pgut001/cryptlib/) for
[debian](http://debian.org/). My conclusion: it may not be worth my
time.

One of the main reasons i wanted to package it for debian is because i
would like to see more
[OpenPGP](https://tools.ietf.org/html/rfc4880)-compliant free software
available to debian users and developers, particularly if the code is
GPL-compatible.

Cryptlib makes it quite clear that [it intends to be
GPL-compatible](http://www.cs.auckland.ac.nz/~pgut001/cryptlib/download.html):

> cryptlib is distributed under a dual license that allows free,
> open-source use under a GPL-compatible license and closed-source use
> under a standard commercial license.

However, a significant portion of the cryptlib codebase (particularly
within the `bn/` and `crypt/` directories) appears to derive directly
from OpenSSL, and it retains Eric Young's copyright and licensing. This
licensing retains the so-called "advertising clause" that [is generally
acknowledged to be deliberately incompatible with the
GPL](http://people.gnome.org/~markmc/openssl-and-the-gpl.html). (A
common counterargument for this incompatibility is that OpenSSL should
be considered a "System Library" for
[GPL](http://www.gnu.org/licenses/gpl.html)'ed tools that link against
it; whether or not you believe this for tools linked against OpenSSL,
this counterargument clearly does not hold for a project that embeds and
ships OpenSSL code directly, as CryptLib does)

This does not mean that CryptLib is not free software (it is!), nor does
it mean that you cannot link it against GPL'ed code (you can!). However,
you probably can't *distribute* the results of linking CryptLib against
any GPL'ed code, because the GPL is incompatible with the OpenSSL
license.

The un-distributability of derived GPL-covered works makes the CryptLib
package much less appealing to me, since i want to be able to write
distributable code that links against useful libraries, and many of the
libaries i care about happen to be under the GPL.

I also note that Peter Gutmann and [CryptLib Security
Software](http://cryptlib.com) (apparently some sort of company set up
to distribute CryptLib) may be in violation of Eric Young's License,
which states:

>      * 3. All advertising materials mentioning features or use of this software *    must display the following acknowledgement: *    "This product includes cryptographic software written by *     Eric Young (eay@cryptsoft.com)" *    The word 'cryptographic' can be left out if the rouines from the library *    being used are not cryptographic related :-).

A [Google search for Eric Young's name on
cryptlib.com](https://encrypted.google.com/search?q=site%3Acryptlib.com%20%27eric%20young%27)
yields no hits. I do find his name on page 340 of [the Cryptlib
manual](http://www.cryptlib.com/downloads/manual.pdf), but that hardly
seems like it covers "All advertising materials mentioning features or
use of this software". I suppose it's also possible that CryptLib has
negotiated an alternate license with Eric Young that I simply don't know
about.

In summary: i think CryptLib could go into debian, but its incorporation
of OpenSSL-licensed code makes it less useful than i was hoping it would
be.

I have a few other concerns about the package after looking it over in
more detail. I suspect these are fixable one way or another, but i
haven't sorted them out yet. I'm not sure i'll spend the time to do so
based on the licensing issues i sorted out above.

But in case anyone feels inspired, the other concerns i see are:

embedded code copies
:   In addition to embedded (and slightly-modified) copies of `bn/` and
    `crypt/` from OpenSSL, CryptLib contains an embedded copy of zlib.
    [Debian has good reasons](http://bugs.debian.org/392362) for wanting
    [to avoid embedded code
    copies](http://www.debian.org/doc/debian-policy/ch-source.html#s-embeddedfiles).
    I've got it CryptLib building against the system copy of zlib, but
    the bits copied from OpenSSL seem to be more heavily patched, which
    might complicate relying on the system version.

executable stack

:   lintian reports
    [shlib-with-executable-stack](http://lintian.debian.org/tags/shlib-with-executable-stack.html)
    for the library after it is built. I don't fully understand what
    this means or how to fix it, but i suspect it derives from the
    following set of object files that also have an executable stack:

    ``` {.terminal}
    0 dkg@pip:~/src/cryptlib/cryptlib$ for foo in $(find . -iname '*.o'); do>  if ! readelf -S "$foo" | grep -q .note.GNU-stack ; then>   echo "$foo">  fi> done./shared-obj/desenc.o./shared-obj/rc5enc.o./shared-obj/md5asm.o./shared-obj/castenc.o./shared-obj/bfenc.o./shared-obj/rmdasm.o./shared-obj/sha1asm.o./shared-obj/bn_asm.o./shared-obj/rc4enc.o0 dkg@pip:~/src/cryptlib/cryptlib$ 
    ```

    They probably all need to [be
    fixed](http://www.gentoo.org/proj/en/hardened/gnu-stack.xml), unless
    there's a good reason for them to be that way (in which case they
    need to be documented).

non-PIC code
:   lintian reports
    [shlib-with-non-pic-code](http://lintian.debian.org/tags/shlib-with-non-pic-code.html)
    on the library after it is built. I don't fully understand what this
    means, or how to resolve it cleanly, but i imagine this is just a
    question of sorting out the details.

Anyone interested in using my packaging attempts as a jumping off point
can start with:

    git clone git://lair.fifthhorseman.net/~dkg/cryptlib

Oh, and please correct me about anything in this post; I'd be especially
happy to hear that my analysis of the licensing issues above is wrong :)

**Tags**: [cryptlib](https://debian-administration.org/tag/cryptlib),
[gpl](https://debian-administration.org/tag/gpl),
[licensing](https://debian-administration.org/tag/licensing),
[openssl](https://debian-administration.org/tag/openssl)

</p>

