Title: Protecting Software Updates
Date: 2018-06-28 11:57-04:00
Author: Daniel Kahn Gillmor (dkg)
Slug: protecting-software-updates

In my work at the [ACLU](https://www.aclu.org), we fight for civil
rights and civil liberties.  This includes the ability to communicate
privately, free from surveillance or censorship, and to control your
own information.  These are principles that I think most free software
developers would agree with.  In that vein, we [just released a guide
to securing software update
channels](https://www.aclu.org/blog/privacy-technology/surveillance-technologies/big-brother-getting-more-tech-savvy-heres-how)
in collaboration with students from [NYU Law
School](http://www.law.nyu.edu/academics/clinics/semester/technologylawandpolicy).

The guide focuses specifically on what people and organizations that
distribute software can do to ensure that their software update
processes and mechanisms are actually things that their users can
reliably trust.  The goal is to make these channels trustworthy, even
in the face of attempts by government agencies to force software
vendors to ship malware to their users.

Why software updates specifically? Every well-engineered system on
today's Internet will have a software update mechanism, since there
are inevitably bugs that need fixing, or new features added to improve
the system for the users. But update channels also represent a risk:
they are an unclosable hole that enables installation of arbitrary
software, often at the deepest, most-privileged level of the machine.
This makes them a tempting target for anyone who wants to force the
user to run malware, whether that's a criminal organization, a
corporate or political rival, or a government surveillance agency.

I'm pleased to say that Debian has already implemented many of the
technical recommendations we describe, including leading the way on
[reproducible builds](https://reproducible-builds.org).  But as
individual developers we might also be targeted, as [lamby points
out](https://media.libreplanet.org/u/libreplanet/m/you-think-you-re-not-a-target-a-tale-of-three-developers/),
and it's worth thinking about how you'd defend your users from such a
situation.

As an organization, it would be great to see Debian continue to expand
its protections for its users by holding ourselves even more
accountable in our software update mechanisms than we already do.  In
particular, I'd love to see work on binary transparency, similar to
[what Mozilla has been
doing](https://wiki.mozilla.org/Security/Binary_Transparency), but
that ensures that the archive signing keys (which our users trust)
can't be abused/misused/compromised without public exposure, and that
allows for easy monitoring and investigation of what binaries we are
actually publishing.

In addition to technical measures, if you think you might ever get a
government request to compromise your users, please make sure you are
in touch with a lawyer who has your back, who knows how to challenge
requests in court, and who understands why software update channels
should not be used for deliberately shipping malware.  If you're
facing such a situation, and you're in the USA and you don't have a
lawyer yet yourself, you can [reach out to the lawyers my workplace,
the ACLU's Speech, Privacy, and Technology Project for
help](mailto:SPTIntake@aclu.org?subject=Software+updates).

Protecting software update channels is the right thing for our users,
and for free software -- Debian's priorities.  So please take a look
at [the
guidance](https://www.aclu.org/issues/privacy-technology/consumer-privacy/how-malicious-software-updates-endanger-everyone),
think about how it might affect you or the people that you work with,
and start a conversation about what you can do to defend these systems
that everyone is obliged to trust on today's communications.
