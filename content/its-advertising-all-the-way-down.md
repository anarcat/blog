Title: It's Advertising all the way down
Date: 2013-05-03 17:21
Author: Daniel Kahn Gillmor (dkg)
Slug: its-advertising-all-the-way-down

Today i saw a billboard on the side of a bus. It was from a cable TV
channel, bragging about how well-connected their viewers are (presumably
on the internet, social media, blogs, etc).

It shows a smiling, attractive man, with text next to him saying
something like "I told 9000 people what smartphone to buy".

What happened here?

-   A TV channel bought an ad on the side of a bus
-   trying to demonstrate to other advertisers
-   about how good their viewers are at providing advertising-by-proxy
-   on services that themselves are mostly advertising platforms
-   to sell devices that are themselves often used for advertising
    delivery.

And almost all of these steps count as positive economic activity when
we try to measure whether the US economy is healthy.

I am depressed by this tremendous waste of time and effort.

**Tags**:
[advertising](https://debian-administration.org/tag/advertising)

</p>

