Title: proprietary software activation fail
Date: 2013-02-01 16:25
Author: Daniel Kahn Gillmor (dkg)
Slug: proprietary-software-activation-fail

i have a colleague who is forced by work situations to use Windows.
Somehow, I'm the idiot\^W\^W\^W\^W\^Wfriendly guy who gets tapped to fix
it when things break.

Well, this time, the power supply broke. As in, dead, no lights, no fan,
no nothing. No problem, though, the disk is still good, and i've got a
spare machine lying around; and the spare is actually superior hardware
to the old machine so it'll be an upgrade in addition to a fix. Nice! So
i transplant the disk and fire up the new chassis.

But WinXP fails to boot with a lovely "0x0000007b" BSOD. The [internet
tells me](http://support.microsoft.com/kb/324103) that this might mean
it can't find its own disk. OK, pop into the new chassis' BIOS, tell it
to run the SATA ports in "legacy IDE" mode, and try again.

Now we get a "0x0000007e" BSOD. Some [digging on the
'net](http://support.microsoft.com/kb/330182) makes me think it's now
complaining now about the graphics driver. Hmm. Well, i figure i can
probably work around that by installing new drivers from Safe Mode. So i
reboot into Safe Mode.

Success! It boots to the login screen in Safe Mode. And, mirabile dictu,
i happen to know the Administrator password. I put it in, and get a
message that this Windows installation isn't "activated" yet --
presumably because the hardware has changed out from under it. And by
the way, i'm not allowed to log in via safe mode until it's activated.
So please reboot to "normal" Windows and activate it first.

Except, of course, the whole reason i'm booting into safe mode was
because normal Windows gives a BSOD. Grrrr. Who thought up this
particular lovely catch-22?

OK, change tactics. Scavenging the scrap bin turns up a machine with a
failed mainboard, but a power supply with all the right leads. It's
rated for about 80W less than the old machine's failed supply, but i
figure if i rip out the DVD-burner and the floppy drive maybe it will
hold. Oh, and the replacement power supply doesn't physically fit the
old chassis, but it hangs halfway out the back and sort of rattles
around a bit. I sacrifice the rest of the scrap machine, rip out its
power supply, stuff the power supply into the old chassis, swap the
original disk back in, and ... it boots successfully, finally.

That was the shorter version of the story :P

So now my colleague has a horrible mess of a frankencomputer which is
more likely to fail again in the future, instead of a nice shiny
upgrade. Why? Because Microsoft's need to control the flow of software
takes priority over the needs of their users.

This is what you get when you let Marketing and BizDev drive your
technical decisions.

Do i still need to explain why i prefer free software?

**Tags**: [griping](https://debian-administration.org/tag/griping),
[windows](https://debian-administration.org/tag/windows)

</p>

