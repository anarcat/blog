Title: trouble with umn.edu (ftp.us.debian.org) mirror?
Date: 2007-03-26 16:41
Author: Daniel Kahn Gillmor (dkg)
Slug: trouble-with-umnedu-ftpusdebianorg-mirror

is anyone else having trouble accessing the debian mirror at
`debian-mirror.mirror.umn.edu`?

I'm in the US, and `ftp.debian.org` is resolving to this host for me,
but the host itself is not responding (since last night, if my
`cron-apt` logs are accurate).

I haven't been able to find any contact info about that mirror in a
brief couple searches. Is there someone i should contact (either at
debian, to repoint the DNS, or at UMN, to fix the server)?
